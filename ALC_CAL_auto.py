# -*- coding: utf-8 -*-
#
# Warning It does not work with L1 files from UK met office
# UK Met provides NOT-Range-Corrected-Signal instead of RCS



from __future__ import print_function, division, absolute_import

from ALC_cal_rayleigh_tools_v3 import alc_cal_rayleigh
from datetime import timedelta, date
from utils import nans
from matplotlib.pyplot import *
from matplotlib import cm
import numpy as np
import json

#from accelerate import profiler
#p = profiler.Profile(signatures=False)
#p.enable()

# def daterange(start_date, end_date):
    # for n in range(int ((end_date - start_date).days)):
        # yield start_date + timedelta(n)

close('all')

#%%  Inputs """
# start_date = date(2017,1,15)
# end_date = date(2017,2,6 )

# Define instrument information
with open('E-PROFILE_instruments_L2_auto.json') as data_file:    
    info = json.load(data_file)   
  


# Define Rayleigh calibration options
with open('E-PROFILE_options_calibration_auto.json') as data_file_options:    
    options = json.load(data_file_options)

# Flag meanings    
flag_list={ '1':'Successfull',
    '0.5':'is_partially_clear_night',
    '-1':'Not a clear night',
    '0':'No data',
    '-2':'Signal is not proportional to Molecular scattering (Rayleigh fit give different results compared to Median Sl/Pmol)',
    '-3':'Other quality ratio',
    '-4':'Missing Model data',
    '-5':'RCS contains only NaN',
    '-6':'Uncertainties higher than CL Values',
    '-7': 'Negative Rayleigh fit',
    '-8': 'Rayleigh fit issue: a is Larger than b'}    


#%% Loop on station
#calc length of date range for preallocation 

CL=nans((len(info)))
flag=nans((len(info)))
Error_CL = nans((len(info)))

yesterday = date.today() - timedelta(days=1)
date_str= yesterday.strftime("%Y%m%d")
print(date_str)
for s in range(0,len(info)):
	print('------------------------------   ' + info[s]['SiteName'] + '  ---------------------------------' )
	if info[s]["Type"] == 'CL51':
		print('No calibration')
	elif info[s]["Type"] == 'CL31':
		print('No calibration')
	elif info[s]["Type"] == 'CHM15k':
		CL[s], flag[s],Error_CL[s]=alc_cal_rayleigh(date_str,info[s],options)  
	elif info[s]["Type"] == 'Mini-MPL':
		CL[s], flag[s],Error_CL[s]=alc_cal_rayleigh(date_str,info[s],options)     
	else:
		raise NotImplementedError(" Implement Calibration for this instrument")         
#[CL==-1]=np.nan
Error_CL[Error_CL==-1]=np.nan

#p.disable()
#p.print_stats()
#profiler.plot(p)