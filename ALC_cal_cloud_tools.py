# -*- coding: utf-8 -*-
"""
Created on Wed Jul 15 14:17:45 2015

author: hem
"""


from __future__ import print_function, division, absolute_import
import sys
from netCDF4 import Dataset
import numpy as np
import pylab as pl
from matplotlib.pyplot import *
import matplotlib.colorbar 

import math
import datetime
from scipy.interpolate import interp1d
from scipy.stats import linregress

from utils import nans
import time as timing
import os.path

#import ipdb  # ipdb.set_trace()
    
date="20150710"

# Define instrument information
info={
    'instrument_folder':"cl51_010226",
    'instrument':"CL51RAO",
    'folder_ECMWF':"E:/ECMWF/",
    'folder_root':"M:/pay-data/data/pay/REM/ACQ/E_PROFILE_PILOT/DATA/CEILINEX/", 
    'lat':52.21,
    'lon':14.12,
    }

# Define Cloud calibration options
options={
    'plot_main':1,
    'plot_all':1,
    'LRaer':52,
    'z_high_cloud':5000,
    'z_low_cloud':1000,    
    'min_time_range':5,    
    }


resol_min=5
resol_z=15
#%% Load data """
# Read data around 00:00 between hour_min and hour_max
# Data are stored in two files : nc_file and nc_file_previousday
start = timing.time()
date_nb=int(date)
date_nb_previousday=date_nb-1
date_previousday=str(date_nb_previousday)
folder=info["folder_root"] + info["instrument_folder"] +"/nc/"
nc_file = folder + date + "_lindenberg_" + info["instrument"] + ".nc"

print ("read: " + nc_file)
if not(os.path.isfile(nc_file) and os.path.isfile(nc_file)):
    raise IOError("Missing file")

data = Dataset(nc_file)

# Read time 
time = data.variables['time'][:]

if data.variables['time'].units=="seconds since 1970-01-01 00:00:00":
    hours = (time-time[0])/3600
    hours_since_start=(time-time[0])/3600
else:
    raise NotImplementedError("Define time format")


# Read range and altitude
if data.variables['range'].shape==data.variables['range'].shape:
    range_alc = data.variables['range'][:]
else: 
    raise Exception("Change in Range shape")
    
str1=data.altitude
altitude=int(filter(unicode.isdigit, str1)) # Convert String to integer 
z=range_alc+altitude

# Read Range corrected signal
rcs=data.variables['rcs_910'][:]
#Read Cloud base height
cbh = data.variables['cb'][:].astype(float)

data.close

print("Reading Done")
   # ipdb.set_trace()
#%% Plot data """
if options["plot_main"]==1 or options["plot_all"]==1:
    print("Plot")
    figure()
    pcolormesh(hours_since_start,range_alc,rcs.conj().transpose(), vmin=0, vmax=100)
    title(date + " - " + info["instrument"] )
    hc=colorbar()
    xlabel("Hours since start")
print (timing.time() - start)

#%% Average
#resol_z=np.unique(np.diff(range_alc))
#if len(resol_z)>1:
#    raise IOError("More than one stez in altitude")
    
    
hours_min=min(hours);
range_min=np.floor(min(range_alc)/resol_z)*resol_z

hours_mean = np.arange(hours_min,max(hours),resol_min/60)
range_mean =  np.arange(range_min,max(range_alc),resol_z)
rcs_mean_tmp  =  nans((len(hours_mean),len(range_alc)))
rcs_mean = nans((len(hours_mean),len(range_mean)))
cbh[cbh<=0]=np.nan
min_cloud =np.zeros((len(hours_mean),1));
for i in range(0,len(hours_mean)):
    index = np.logical_and(hours>=hours_mean[i] , hours<hours_mean[i]+resol_min/60)
    if any(index):
        rcs_mean_tmp[i,:] = np.nanmean(rcs[index,:],0);
        #Consider that the cloud base height is the lowest altitude of the cloud
        min_cloud[i,0]= min(cbh[index,0]);
    


for i in range(0,len(range_mean)):
    index=np.logical_and(range_alc>=range_mean[i],range_alc<range_mean[i]+resol_z)
    if any(index):
        rcs_mean[:,i] = np.nanmean(rcs_mean_tmp[:,index],1);
        
        
figure()
subplot(2,1,1)
pcolormesh(hours_since_start,range_alc,rcs.conj().transpose(), vmin=0, vmax=100)
plot(hours,cbh)
title(date + " - " + info["instrument"] )
hc=colorbar()
xlabel("Hours since start")


subplot(2,1,2)
pcolormesh(hours_mean,range_mean,rcs_mean.conj().transpose(), vmin=0, vmax=100)
title(date + " - " + info["instrument"] )
hc=colorbar()
plot(hours_mean,min_cloud)

xlabel("Hours since start")
    

#%% Check if cloudy night
no_cloud_value=-9
is_clear_night=np.all(np.logical_or(cbh==no_cloud_value, cbh<options["z_high_cloud"]))
    
profiles_per_min=np.round(1/((hours_since_start[1]-hours_since_start[0])*60));
if not(is_clear_night):
    index_cloud=np.logical_not(np.logical_or(cbh[:,0]==no_cloud_value, cbh[:,0]>options["z_low_cloud"]))
    if len(index_cloud)-len(np.nonzero(index_cloud)[0])>options["min_time_range"]*60*profiles_per_min:

        cloudy_profile=np.zeros(index_cloud.shape, dtype=bool);
    
        #remove profiles with no cloud
        print(str(sum(cloudy_profile)) + ' Profiles removed on ' + str(len(cloudy_profile)))
        if all(cloudy_profile):
            is_clear_night=0;
            is_partially_clear_night=0;
            flag=0;
        else:            
            cbh=np.delete(cbh,np.nonzero(not(cloudy_profile))[0],0)
            time=np.delete(time,np.nonzero(not(cloudy_profile))[0],0)
            rcs=np.delete(rcs,np.nonzero(not(cloudy_profile))[0],0)
            hours_since_start=np.delete(hours_since_start,np.nonzero(not(cloudy_profile))[0])
            hours=np.delete(hours,np.nonzero(not(cloudy_profile))[0])        


            
        if cloudy>0:
            print(str(cloudy) + ' Profiles partially removed above '  + str(options["z_low_cloud"]) + 'm')
        
        is_partially_clear_night=1;
        flag=0.5;
    else:
        print("Not a clear night")
        is_partially_clear_night=0;
        
        
    print("Plot")
    figure()
    pcolormesh(hours_since_start,range_alc,rcs.conj().transpose(), vmin=0, vmax=100)
    title(date + " - " + info["instrument"] )
    hc=colorbar()
    xlabel("Hours since start")

#%%  Load Temperature and pressure profiles """
print ("Load Temperature and pressure profiles")
nc_file_ECMWF = info["folder_ECMWF"] +   "MACC_" + date  + ".nc"
print ("read: " + nc_file_ECMWF)

data_ECMWF = Dataset(nc_file_ECMWF)
lat = data_ECMWF.variables['latitude'][:]
lon = data_ECMWF.variables['longitude'][:]

# Find closer to data point
dist_lon=(abs(lon-info["lon"]))
index_min_lon=dist_lon.argmin()

dist_lat=(abs(lat-info["lat"]))
index_min_lat=dist_lat.argmin()

T_raw = data_ECMWF.variables['t'][:,:,index_min_lat,index_min_lon]
time_ecmwf = data_ECMWF.variables['time'][:]
level = data_ECMWF.variables['level'][:]
z_ecmwf = data_ECMWF.variables['z'][:,:,index_min_lat,index_min_lon]/9.80655 # GEopotential height in meter
data_ECMWF.close

# Interpolate to ALC resolution
T=np.zeros((len(time_ecmwf),len(range_alc)))
P=np.zeros((len(time_ecmwf),len(range_alc)))

for i in range(0,len(time_ecmwf)):
    T[i,:]=interp1d(z_ecmwf[i,:],T_raw[i,:], bounds_error=False, fill_value=np.nan)(z)
    P[i,:]=interp1d(z_ecmwf[i,:],level, bounds_error=False, fill_value=np.nan)(z)
    # REplace first NaN by value above
    ind = np.where(~np.isnan(T[i,:]))[0]
    T[i,:ind[0]] = T[i,ind[0]]
    P[i,:ind[0]] = P[i,ind[0]]


if options["plot_all"]==1:
    print("Plot ECMWF")
    figure()
    subplot(2,2,1)
    pcolormesh(time_ecmwf,level,T_raw.conj().transpose())
    title(" ECMWF MACC")
    hc=colorbar()
    
    subplot(2,2,2)
    plot(T_raw.conj().transpose(),level)
    title(" ECMWF MACC")
    
    
    subplot(2,2,3)
    pcolormesh(time_ecmwf,z,T.conj().transpose(), vmin=200, vmax=300)
    title(" ECMWF MACC")
    hc=colorbar()
    
    subplot(2,2,4)
    plot(T.conj().transpose(),z)
    title(" ECMWF MACC")
    tight_layout()

print( timing.time() - start)

print("Done")
    
    