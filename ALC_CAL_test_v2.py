# -*- coding: utf-8 -*-
"""
Rayleigh Calibration for automated Lidar and Ceilometers.
Tested on CHM15k and CL51. It should not be used on CL51 before problems with signal distortion are solved
Created on Wed Jul 15 14:17:45 2015

author: hem
"""

from __future__ import print_function, division, absolute_import
# import sys
from netCDF4 import Dataset
import numpy as np
# import pylab as pl
from matplotlib.pyplot import *
# import matplotlib.colorbar
import copy
import math
# import datetime
from scipy.interpolate import interp1d
from scipy.stats import linregress

from utils import nans
import time as timing
import calendar
import os.path
import glob
import re


date_str="20160320"

#    # Define instrument information
info= {    'name': 'SAINT-CHRISTOPHE_AOSTA',
    'instrument':"16054", # WMO Number, ICAO or other
    'free_format':"CHM150104", # Free format used by provider
    'identifier':"0",
    'instrument_type':'Jenoptik CHM15k',
    'folder_ECMWF':"E:/ECMWF/",
    'folder_root':"C:/DATA/Ceilometer/E-PROFILE_L1_FILES/", 
    'folder_output':"C:/DATA/Ceilometer/E-PROFILE_L1_FILES/Calibration/",
    'lat':45.7422,
    'lon':7.357   }

date_str="20160424"
 
info=  {    'name': 'LERWICK',
    'instrument':"03005", # WMO Number, ICAO or other
    'free_format':"CHM110109", # Free format used by provider
    'identifier':"C",
    'instrument_type':'Jenoptik CHM15k',
    'folder_ECMWF':"E:/ECMWF/",
    'folder_root':"C:/DATA/Ceilometer/E-PROFILE_L1_FILES/", 
    'folder_output':"C:/DATA/Ceilometer/E-PROFILE_L1_FILES/Calibration/",
    'lat':60.133,
    'lon':-1.183,
    }
    

#    
#    # Define Rayleigh calibration options
options={
    'hour_min': 20, # Hour to start calibration on previous day (Should be 21)
    'hour_max': 4, # Hour to finish calibration (Should be 03)
    'min_time_range':1, # Minimum hours where you have non cloudy data (Default 3)
    'plot_main': 1,
    'plot_all':  0,
    'z_low_cloud':3000,
    'LRaer':52,
    'z_start_ext':100,
    'calc_ext_above_molecular':0,
    'subtract_background':0,
    'consider_points_lower_than_molecular':1,
    'threshold_quality':75, # Default 15
    }
#    # options to find best molecular zone       
options_half_length_m = range(250, 2000, 240)
options_range_alc_start_m = 2000
options_range_end_m = 6000
options_rayleigh_fit_range_increment = 8  # Increment in bin (improve speed but reduce possible windows)

# def alc_cal_rayleigh(date_str,info,options) :
# %% Load data
# Read data around 00:00 between hour_min and hour_max
start = timing.time()

# Select Only Data in the time range
date_nb_epoch = calendar.timegm(timing.strptime(date_str, '%Y%m%d'))
date_nb = int(date_str)
date_nb_previousday = date_nb - 1
date_previousday = str(date_nb_previousday)
date_nb_nextday = date_nb + 1
date_nextday = str(date_nb_nextday)
date_min = int(date_previousday + "%02d" % options["hour_min"] + "0000")
date_max = int(date_str + "%02d" % options["hour_max"] + "0000")
#    C:/DATA/Ceilometer/E-PROFILE_L1_FILES/20160414000000-20160415000000-L1/L1_FILES
#    C:\DATA\Ceilometer\E-PROFILE_L1_FILES\201604150000-201604160000-L1\L1_FILES
folder= info["folder_root"] + date_str + "0000" + "-" + date_nextday + "0000-L1/L1_FILES/"
folder_previous= info["folder_root"] + date_previousday + "0000" + "-" + date_str + "0000-L1/L1_FILES/"

nc_file =  folder +   "eprofile_l1_" + info["instrument"] + "_" + info["identifier"] + date_str + "*_" + \
          info["free_format"] + ".nc"
nc_file_previousday = folder_previous  + "eprofile_l1_" + info["instrument"] + "_" + info[
    "identifier"] + date_previousday + "*_" + info["free_format"] + ".nc"

file_list_raw = glob.glob(nc_file_previousday) + glob.glob(nc_file)
file_list = []
for i in range(0, len(file_list_raw)):
    date_str_tmp = re.findall("_" + info["identifier"] + '(.+?)_', file_list_raw[i])
    date_nb = int(date_str_tmp[0] + "00")

    if (date_nb >= date_min) & (date_nb < date_max):
        file_list = file_list + [file_list_raw[i]]

if len(file_list) == 0:
    flag = 0
    CL = -1    
    print("No file")
    print(nc_file)
    print(nc_file_previousday)
    Error_CL = 0
    stop 
else:
    print("read: " + str(len(file_list)) + " files between " + str(date_min) + " and " + str(date_max))

for i in range(0, len(file_list)):
    # print(file_list[i])
    data = Dataset(file_list[i])

    # Read time 
    if i == 0:
        time = data.variables['time'][:]
    else:
        time = np.concatenate((time, data.variables['time'][:]), axis=0)

    # Read range and altitude
    if i == 0:
        range_alc = data.variables['range'][:]
        altitude = data.variables['altitude'][:]
        z = range_alc + altitude
    else:
        if data.variables['range'].shape != range_alc.shape:
            raise Exception("Change in Range shape")

    # Read Range corrected signal
    if i == 0:
        rcs = data.variables['rcs_0'][:]
    else:
        rcs = np.concatenate((rcs, data.variables['rcs_0'][:]), axis=0)

    # Read Cloud base height
    if i == 0:
        cbh = data.variables['cbh'][:]
    else:
        cbh = np.concatenate((cbh, data.variables['cbh'][:]), axis=0)

    # Read Optical module temperature
    if i == 0:
        temp_lom = data.variables['temp_lom'][:]
    else:
        temp_lom = np.concatenate((temp_lom, data.variables['temp_lom'][:]), axis=0)

    # Read window transmission
    if i == 0:
        window_transmission = data.variables['state_optics'][:]
    else:
        window_transmission = np.concatenate((window_transmission, data.variables['state_optics'][:]), axis=0)

    # Read Status laser
    if i == 0:
        state_laser = data.variables['state_laser'][:]
    else:
        state_laser = np.concatenate((state_laser, data.variables['state_laser'][:]), axis=0)

    # Read Status detector
    if i == 0:
        state_detector = data.variables['state_detector'][:]
    else:
        state_detector = np.concatenate((state_detector, data.variables['state_detector'][:]), axis=0)
    # Read Laser life time
    if i == 0:
        life_time = data.variables['life_time'][:]
    else:
        life_time = np.concatenate((life_time, data.variables['life_time'][:]), axis=0)
    # Read calibration_pulse
    if i == 0:
        calibration_pulse = data.variables['p_calc'][:]
    else:
        calibration_pulse = np.concatenate((calibration_pulse, data.variables['p_calc'][:]), axis=0)

    # Read attributes
    if i == 0:
        instrument_id = data.instrument_id
        software_id = data.software_id


    # Close netcdf file
    data.close
    
# For old software version
if info["instrument_type"] == 'Jenoptik CHM15k':        
    if software_id < 0.7:        
        rcs_raw=copy.deepcopy(rcs)
        range_alc_mat=np.tile(range_alc,(len(time),1))
        rcs=rcs_raw * range_alc_mat * range_alc_mat 
        

hours = (time - min(time)) * 24
hours_since_start = (time - min(time)) * 24
print("Reading Done")

# %% Plot data
no_cloud_value = -1

if options["plot_main"] == 1 or options["plot_all"] == 1:
    print("Plot")
    figure(figsize=(10,5))
    subplot(1,2,1)

    #pcolormesh(hours_since_start, range_alc, rcs.conj().transpose(), vmin=1e4, vmax=1e6)
    rcs_pos=copy.deepcopy(rcs.conj().transpose())
    rcs_pos[rcs_pos<= 0]=np.nan
    lrcs=np.log10(rcs_pos);
    pcolormesh(hours_since_start, range_alc, lrcs, vmin=0, vmax=6)
    #pcolormesh(hours_since_start, range_alc, lrcs,vmin=0 ,vmax=0.1)

    hold(True)
    tmp= copy.deepcopy(cbh)
    tmp[tmp== no_cloud_value]=np.nan
    plot(hours_since_start,tmp,'.',color='grey')  
    title(date_str + " - " + info["instrument"])
    hc = colorbar()
    lim_x=xlim()
    xlabel("Hours since start")
        
print('Time elapsed: ' + str(np.floor(timing.time() - start)) + 's')

   # %% Check if cloudy night
is_clear_night = np.all(cbh == no_cloud_value)
profiles_per_min = np.round(1 / ((hours_since_start[1] - hours_since_start[0]) * 60))
if not (is_clear_night):
    index_cloud = np.logical_not(np.logical_or(cbh[:, 0] == no_cloud_value, cbh[:, 0] > options["z_low_cloud"]))
    if (len(index_cloud) - len(np.nonzero(index_cloud)[0])) > options["min_time_range"] * 60 * profiles_per_min:
        # remove data if cloud 15 min after or before
        contaminated_profile = np.zeros(index_cloud.shape, dtype=bool)
        for ii in range(0, len(time)):
            if np.any(index_cloud[ max(0, -profiles_per_min * 15 + ii + 1):min(ii + profiles_per_min * 15, len(time))]):
                contaminated_profile[ii] = 1
            else:
                contaminated_profile[ii] = 0
    
        # remove profiles with low cloud
        print(str(sum(contaminated_profile)) + ' Profiles removed on ' + str(len(contaminated_profile)))
        if len(time) - sum(contaminated_profile) < options["min_time_range"] * 60 * profiles_per_min:
            is_clear_night = 0
            is_partially_clear_night = 0
            flag = -1
            CL = -1
            Error_CL = 0
            show()
            print("Not a clear night (Only " + str(len(index_cloud) - sum(contaminated_profile)) +" profiles remaining after low cloud)")
            stop
    
        else:
            cbh = np.delete(cbh, np.nonzero(contaminated_profile)[0], 0)
            time = np.delete(time, np.nonzero(contaminated_profile)[0], 0)
            rcs = np.delete(rcs, np.nonzero(contaminated_profile)[0], 0)
            hours_since_start = np.delete(hours_since_start, np.nonzero(contaminated_profile)[0])
            hours = np.delete(hours, np.nonzero(contaminated_profile)[0])
        
        # remove data 500m below cloud level
        cloudy = 0
        for ii in range(0, len(time)):
            if not (cbh[ii, 0] == no_cloud_value):
                rcs[ii, range_alc >= cbh[ii, 0] - 500] = np.nan
                cloudy = cloudy + 1
                
        if len(time) - cloudy < options["min_time_range"] * 60 * profiles_per_min:
            is_clear_night = 0
            is_partially_clear_night = 0
            flag = -1
            CL = -1
            Error_CL = 0
            show()
            print("Not a clear night (Only " + str(len(time) - cloudy) + " profiles remaining after high cloud removal)")
            stop
        if cloudy > 0:
            print(str(cloudy) + ' Profiles partially removed above ' + str(options["z_low_cloud"]) + 'm')
    
        is_partially_clear_night = 1
        flag = 0.5
    else:
        print("Not a clear night ")
        is_partially_clear_night = 0
        flag = -1
        CL = -1
        Error_CL = 0
        show()
        stop
    #            raise NotImplementedError("Cloudy night")
#
#        if options["plot_main"] == 1:
#            print("Plot")
#            figure()
#            pcolormesh(hours_since_start, range_alc, rcs.conj().transpose(), vmin=1e4, vmax=1e6)
#            title(date_str + " - " + info["instrument"])
#            hc = colorbar()
#            xlabel("Hours since start")
        
if options["plot_main"] == 1 or options["plot_all"] == 1:
    subplot(1,2,2)
    rcs_pos=copy.deepcopy(rcs.conj().transpose())
    rcs_pos[rcs_pos<= 0]=np.nan
    lrcs=np.log10(rcs_pos);
    pcolormesh(hours_since_start, range_alc, lrcs, vmin=0, vmax=6)
    title(date_str + " - " + info["instrument"])
    hc = colorbar()
    xlabel("Hours since start")
    xlim(lim_x)
    show()

# %%  Load Temperature and pressure profiles """
print("Load Temperature and pressure profiles")
nc_file_ECMWF = info["folder_ECMWF"] + "MACC_" + date_str + ".nc"

if not os.path.isfile(nc_file_ECMWF):
    print("Missing: " + nc_file_ECMWF)
    flag = -4
    CL = -1
    Error_CL = 0
    stop
    
    
print("read: " + nc_file_ECMWF)

data_ECMWF = Dataset(nc_file_ECMWF)
lat = data_ECMWF.variables['latitude'][:]
lon = data_ECMWF.variables['longitude'][:]

# Find closer to data point
dist_lon = (abs(lon - info["lon"]))
index_min_lon = dist_lon.argmin()

dist_lat = (abs(lat - info["lat"]))
index_min_lat = dist_lat.argmin()

T_raw = data_ECMWF.variables['t'][:, :, index_min_lat, index_min_lon]
time_ecmwf = data_ECMWF.variables['time'][:]
level = data_ECMWF.variables['level'][:]
z_ecmwf = data_ECMWF.variables['z'][:, :, index_min_lat, index_min_lon] / 9.80655  # GEopotential height in meter
data_ECMWF.close

# Interpolate to ALC resolution
T = np.zeros((len(time_ecmwf), len(range_alc)))
P = np.zeros((len(time_ecmwf), len(range_alc)))

for i in range(0, len(time_ecmwf)):
    T[i, :] = interp1d(z_ecmwf[i, :], T_raw[i, :], bounds_error=False, fill_value=np.nan)(z)
    P[i, :] = interp1d(z_ecmwf[i, :], level, bounds_error=False, fill_value=np.nan)(z)
    # REplace first NaN by value above
    ind = np.where(~np.isnan(T[i, :]))[0]
    T[i, :ind[0]] = T[i, ind[0]]
    P[i, :ind[0]] = P[i, ind[0]]

if options["plot_all"] == 1:
    print("Plot ECMWF")
    figure()
    subplot(2, 2, 1)
    pcolormesh(time_ecmwf, level, T_raw.conj().transpose())
    title(" ECMWF MACC")
    hc = colorbar()

    subplot(2, 2, 2)
    plot(T_raw.conj().transpose(), level)
    title(" ECMWF MACC")

    subplot(2, 2, 3)
    pcolormesh(time_ecmwf, z, T.conj().transpose(), vmin=200, vmax=300)
    title(" ECMWF MACC")
    hc = colorbar()

    subplot(2, 2, 4)
    plot(T.conj().transpose(), z)
    title(" ECMWF MACC")
    tight_layout()

print('Time elapsed: ' + str(np.floor((timing.time() - start) * 10) / 10) + 's')

# %%  Calculate Molecular Backscatter
# from (Bucholtz, 1995)
print("Calculate Molecular Backscatter")
if info["instrument_type"] == 'Vaisala CL51':
    wavelength = 910 * 1e-9  # nm
elif info["instrument_type"] == 'Vaisala CL31':
    wavelength = 910 * 1e-9  # nm
elif info["instrument_type"] == 'Jenoptik CHM15k':
    wavelength = 1064 * 1e-9  # nm
else:
    raise NotImplementedError("Implement instrument wavelength")

T = T[1, :]
P = P[1, :] * 100  # in Pa
# Constants    
T0 = 273.15 + 15  # K
P0 = 101325 # Pa
rho = 0.0301 # depol factor
N = 2.547e25 # Molecular density(m-3)
R = 8.314418
Navogadro = 6.022005e23
density = Navogadro * P / R / (T)

## refractive index
m = (5791817 / (238.0185 - (1 / (wavelength * 1e6)) ** 2) + 167909 / (
57.362 - (1 / (wavelength * 1e6)) ** 2)) * 1e-8 + 1

## Calc alpha and beta
beta_mol_tot = nans(T.shape)
for i in range(0, len(range_alc)):
    # From Bucholtz 1995
    beta_mol_tot[i] = 24 * np.pi ** 3 * ((m ** 2 - 1) ** 2) * (6 + 3 * rho) / (
    wavelength ** 4 * N ** 2 * (m ** 2 + 2) ** 2 * (6 - 7 * rho)) * N * T0 * P[i] / P0 / T[i]

gamma = rho / (2 - rho)
Pray = 3 / 4 / (1 + 2 * gamma) * (1 + 3 * gamma + (1 - gamma) * math.cos(np.pi) ** 2)

beta_mol = beta_mol_tot / 4 / np.pi * Pray
alpha_mol = beta_mol * 8 * np.pi / 3

## Calc Beta apparant
P_mol = nans(T.shape)
beta_att_mol = nans(T.shape)

trans = nans(T.shape)
dz = z[2] - z[1]

for i in range(0, len(range_alc)):
    trans[i] = math.exp(-sum(alpha_mol[0:i] * dz))
    P_mol[i] = beta_mol[i] * trans[i] * trans[i] / range_alc[i] / range_alc[i]
    beta_att_mol[i] = beta_mol[i] * trans[i] * trans[i]

print('Time elapsed: ' + str(np.floor((timing.time() - start) * 10) / 10) + 's')

# %%  Rayleigh fit
print("Calculate Rayleigh fit")
"""Select data """
index_time1 = hours >= options['hour_max']
index_time2 = hours < options['hour_min']
index_time = np.logical_or(index_time1, index_time2)
rcs_mean = np.nanmean(rcs[index_time, :], axis=0)
if all(np.isnan(rcs_mean)):
    print("RCS contains only NaN")
    flag = -5
    CL = -1
    Error_CL = 0
    stop
    

"""Find best molecular zone """
# Make rayleigh fit with P (not Pr2) to avoid depencies with r2
sl = rcs_mean / range_alc / range_alc
half_length_bin = np.unique(np.floor(options_half_length_m / dz)).astype(int)
range_start_bin = np.floor(options_range_alc_start_m / dz).astype(int)
range_end_bin = np.floor(options_range_end_m / dz).astype(int)
range_bin = range(range_start_bin, range_end_bin, options_rayleigh_fit_range_increment)
range_bin_m = np.array(range_bin) * dz
print(str(len(range_bin) * len(half_length_bin)) + ' possible molecular windows defined')

a = nans((len(range_bin), len(half_length_bin)))
b = nans((len(range_bin), len(half_length_bin)))
r2 = nans((len(range_bin), len(half_length_bin)))
rmse = nans((len(range_bin), len(half_length_bin)))
stderr = nans((len(range_bin), len(half_length_bin)))
p_value_all = nans((len(range_bin), len(half_length_bin)))
sum_abs_b = nans((len(range_bin), 1))

for ii in range(0, len(range_bin)):
    for jj in range(0, len(half_length_bin)):
        x = P_mol[range_bin[ii] - half_length_bin[jj]:min(range_bin[ii] + half_length_bin[jj], len(range_alc))]
        y = sl[range_bin[ii] - half_length_bin[jj]:min(range_bin[ii] + half_length_bin[jj], len(range_alc))]

        if not (np.any(np.isnan(x))) and not (np.any(np.isnan(y))):
            # Make Linear fit
            # Convert x in Mm-1 to keep the same order of Magnitude
            a[ii, jj], b[ii, jj], r_value, p_value_all[ii, jj], stderr[ii, jj] = linregress(x, y)
            r2[ii, jj] = r_value ** 2

    sum_abs_b[ii] = np.sum(np.abs(b[ii, :]))

# Choose altitude where b is minimum for any size of windows
index_min_b = np.argmin(sum_abs_b)
best_range_m = range_bin_m[index_min_b]
best_range = range_bin[index_min_b]

# Chose size of windows where R2 is minimum
ind_max_r2 = np.argmax(r2[index_min_b, :])

best_half_lengh_bin = half_length_bin[ind_max_r2]
best_half_lengh_m = options_half_length_m[ind_max_r2]
# Convert in m-1 
f_results = (a[index_min_b, ind_max_r2], b[index_min_b, ind_max_r2])
error_fit = stderr[index_min_b, ind_max_r2] / a[index_min_b, ind_max_r2]
p_value = p_value_all[index_min_b, ind_max_r2]
best_rmse = rmse[index_min_b, ind_max_r2]
best_r2 = r2[index_min_b, ind_max_r2]
zi = best_range_m - best_half_lengh_m
ze = best_range_m + best_half_lengh_m

ind_molec = np.logical_and(np.logical_and(z >= zi, z <= ze), np.logical_not(np.isnan(rcs_mean)))
# f_results = np.polyfit(beta_mol[ind_molec],rcs_mean[ind_molec], 1)
if options["subtract_background"] == 1:
    sl = sl - f_results[1]

sl = sl / (f_results[0])
beta_att = sl * range_alc * range_alc

 # %% First Quality check
if f_results[0] <= 0:
    print("Negative Rayleigh fit")
    flag = -7
    
    CL = -1
    Error_CL = 0
    stop

if abs(f_results[1]) > f_results[0]:
    print("Rayleigh fit issue: a is Larger than b")
    flag = -8
    CL = -1
    Error_CL = 0
    stop


x = P_mol[range_bin[index_min_b] - half_length_bin[ind_max_r2]: min(range_bin[index_min_b] + half_length_bin[ind_max_r2],
                                                             len(range_alc))]
y = sl[range_bin[index_min_b] - half_length_bin[ind_max_r2]: min(range_bin[index_min_b] + half_length_bin[ind_max_r2],
                                                             len(range_alc))]
aa = np.median(y / x)
p1, p2, k, k, k = linregress(x, y)
error_rel = abs((p1 - aa) / aa * 100)
print('Relative error: ' + str(error_rel))


# Plot
if options["plot_main"] == 1:
    figure(figsize=(12, 10))
    subplot(1, 3, 1)
    plot(rcs_mean, z)
    xlabel('Raw Pr2')

    subplot(1, 3, 2)
    plot(sl * 1e6, z)
    plot(P_mol * 1e6, z)
    plot(xlim(), (zi, zi), 'k--')
    plot(xlim(), (ze, ze), 'k--')
    xscale('log')
    xlabel('P')

    subplot(1, 3, 3)
    plot(beta_att, z)
    plot(beta_att_mol, z)
    plot(xlim(), (zi, zi), 'k--')
    plot(xlim(), (ze, ze), 'k--')
    xlabel('beta att')
    show()
    
    figure()
    plot(x,y,label='Data')
    plot([1e-16,1e-14],[1e-16,1e-14],'-k',label='1:1')
    #plot([1e-16,1e-14],[(1e-16*f_results[0]+f_results[1]),(1e-14*f_results[0]+f_results[1])],'-r',label='fit')
    plot([1e-16,1e-14],[(1e-16*p1+p2),(1e-14*p1+p2)],'--r',label='fit2')

    xlabel('Theorical Molecular Signal')
    ylabel('Signal in molecular region')
    legend()
    #xlim((1e-10,1e-8))
    #ylim((1e-10,1e-8))
    show()
    
if error_rel > options["threshold_quality"]:
    print("Signal is not proportionnal to Molecular scattering (Rayleigh fit give different results compared to Median Sl/Pmol)")
   
    flag = -2
    CL = -1
    Error_CL = 0
    stop

    
if options["plot_all"] == 1:
    figure(figsize=(12, 12))
    ax1 = subplot(4, 1, 1)
    c1 = pcolormesh(np.array(range_bin_m), np.array(options_half_length_m), a.conj().transpose())
    # cbaxes = fig.add_axes([0,1, 0.3, 0.03])
    # cb = plt.colorbar(c, cax = cbaxes)
    hc = colorbar(c1)
    hc.ax.set_ylabel('a')
    xlabel("Altitude of Molecular zone")
    ylabel("Length of the molecular zone")
    xlim((options_range_alc_start_m, options_range_end_m))

    subplot(4, 1, 2)
    c2 = pcolormesh(np.array(range_bin_m), np.array(options_half_length_m), np.abs(b.conj().transpose()))
    hc = colorbar(c2)
    hc.ax.set_ylabel('b')
    xlim((options_range_alc_start_m, options_range_end_m))

    ax3 = subplot(4, 1, 3)
    plot(range_bin_m, sum_abs_b, 'k')
    ylabel('Sum of b coefficient')
    pos1 = ax1.get_position().bounds
    pos3 = ax3.get_position().bounds
    # set the x limits (left and right) to first axes limits
    # set the y limits (bottom and top) to the last axes limits
    newpos = [pos1[0], pos3[1], pos1[2], pos3[3]]
    xlim((options_range_alc_start_m, options_range_end_m))

    ax3.set_position(newpos)

    subplot(4, 1, 4)
    c4 = pcolormesh(np.array(range_bin_m), np.array(options_half_length_m), r2.conj().transpose())
    scatter(best_range_m, best_half_lengh_m)
    hc = colorbar(c4)
    hc.ax.set_ylabel('R^2')
    xlim((options_range_alc_start_m, options_range_end_m));
    show()


# %%  Calc Extinction
print("Calculate Extinction")
LRmol = 8 * np.pi / 3;  # Molecular Lidar Ratio

i_start_ext = np.argmin(abs(range_alc - options["z_start_ext"]))
ind_molec_indexes = np.where(ind_molec)
istart_molec = min(ind_molec_indexes[0])
imax_molec = max(ind_molec_indexes[0])

if options["calc_ext_above_molecular"] == 0:
    imax = imax_molec
else:
    imax = len(range_alc)

Ro = int(np.floor((istart_molec + imax) / 2))
refer = np.mean(beta_att[ind_molec] / beta_mol[ind_molec])

beta_aer = np.zeros(beta_att.shape)
beta_tot = np.zeros(beta_att.shape)
ext_aer = np.zeros(beta_att.shape)
numerator = np.zeros(beta_att.shape)
denominator = np.zeros(beta_att.shape)
aotlidar = 0

# Calcul Klett
for R in range(i_start_ext, imax):
    # integral from RO to  R beta_mol*(Laer-Lmol)
    qt = sum(beta_mol[R:Ro] * (options["LRaer"] - LRmol) * dz)
    numerator[R] = beta_att[R] * math.exp(-2 * qt)
    for r in range(R, Ro):
        # integral from R0 to R Laer*S[R]*T
        T = 0
        for rr in range(r, Ro):  # Calcul of T: integral from Ro to r'
            T = (-2 * beta_mol[rr] * (options["LRaer"] - LRmol) * dz) + T
        denominator[R] = denominator[R] + beta_att[r] * math.exp(T) * options["LRaer"] * dz

    denominator[R] = refer - 2 * denominator[R]
    beta_aer[R] = numerator[R] / denominator[R] - beta_mol[R]
    beta_tot[R] = beta_aer[R] + beta_mol[R]

    if options["consider_points_lower_than_molecular"] == 1:
        ext_aer[R] = beta_aer[R] * options["LRaer"]
        aotlidar = aotlidar + ext_aer[R] * abs(dz)
    else:
        if (beta_tot[R] > beta_mol[R]):
            ext_aer[R] = beta_aer[R] * options["LRaer"]
            aotlidar = aotlidar + ext_aer[R] * abs(dz)
        else:
            beta_tot[R] = beta_mol[R]
            beta_aer[R] = 0
            ext_aer[R] = 0

    if R == i_start_ext:
        ext_aer[1:i_start_ext] = ext_aer[R]
        beta_aer[1:i_start_ext] = beta_aer[R]
        beta_tot[1:i_start_ext] = beta_tot[R]

ext_tot = ext_aer + beta_mol * LRmol

# %%  Calc Calibration constant
print("Calculate Calibration constant from Wiegner and Geiss 2012")
inv_T = np.zeros(range_alc.shape)
Trans = nans(range_alc.shape)
CL_all_altitude = nans(range_alc.shape)

# Modif 02/09/2015.Backgroud from fit (b)  must be subtracted to be homodeneous with Beta tot
if options["subtract_background"] == 1:
    rcs_mean_background_subs_tmp = rcs_mean / range_alc / range_alc
    rcs_mean_background_subs_tmp = rcs_mean_background_subs_tmp - f_results[1]
    rcs_mean_background_subs = rcs_mean_background_subs_tmp * range_alc * range_alc
else:
    rcs_mean_background_subs = rcs_mean

# Modif 02/09/2015:
# - CL must be calculated between PR2 and and Beta attenuated.
# - Integral is calculated with trapz

# Calculate CL at all altitudes
for i in range(0, imax_molec):
    inv_T[i] = np.exp(2 * np.trapz(ext_tot[0:i], range_alc[0:i]))
    Trans[i] = np.exp(-2 * np.trapz(ext_tot[0:i], range_alc[0:i]))
    CL_all_altitude[i] = rcs_mean_background_subs[i] / beta_tot[i] * inv_T[i]

    # Calculate CL only in molecular
CL_all = CL_all_altitude[istart_molec:imax_molec]
CL = np.median(CL_all)
CL_uncertainties = np.std(CL_all) + error_fit * np.median(CL_all)

# %% Quality check
CL_slope_molecular = f_results[0] * inv_T[istart_molec]
Error_CL = abs((CL_slope_molecular - CL) / CL * 100)

if Error_CL > options["threshold_quality"]:
    print("Bad ratio between klett method and slope method: " + str(Error_CL))
    flag = -3
    CL = -1
    Error_CL = 0
    stop

print(("CL: " + str(np.median(CL_all))))
flag = 1

if options["plot_main"] == 1:
    figure()
    subplot(1, 2, 1)
    plot(CL_all_altitude, range_alc, 'b')
    plot((CL, CL), ylim(), 'r')
    plot((CL_slope_molecular, CL_slope_molecular), ylim(), 'k')
    plot((CL - Error_CL * CL / 100, CL - Error_CL * CL / 100), ylim(), '--r')
    plot((CL + Error_CL * CL / 100, CL + Error_CL * CL / 100), ylim(), '--r')
    legend(('CL','Median','Slope method','MEdian-Error','MEdian+Error'))
    plot(xlim(), (zi, zi) - altitude, '--k')
    plot(xlim(), (ze, ze) - altitude, '--k')
    ylabel('Range')
    xlabel('CL')

    subplot(1, 2, 2)
    plot(rcs_mean / np.median(CL) * 1e6, range_alc)
    plot(beta_tot / inv_T * 1e6, range_alc)
    plot(rcs_mean / CL_slope_molecular * 1e6, range_alc, 'k')

    legend(("RCS/CL", r"$\beta_{attenuated}$", "Slope"))
    show()

print('Time elapsed: ' + str(np.floor(timing.time() - start)) + 's')

# %%  Plot results """
print("plotting results")
if options["plot_main"] == 1:
    f=figure(figsize=(12, 8))
    subplot(1, 3, 1)
    ha = plot(beta_att, z)
    plot(beta_mol, z)
    plot(rcs_mean_background_subs / CL_slope_molecular, z)

    plot(xlim(), (zi, zi), 'k--')
    plot(xlim(), (ze, ze), 'k--')
    xlabel("Range Corrected Signal")
    title(("P=" + str(np.median(CL))))

    subplot(1, 3, 2)
    plot(beta_mol * 1e6, z)
    plot(beta_aer * 1e6, z)
    plot(beta_tot * 1e6, z)
    xlabel("Backscatter Coefficient [Mm-1Sr-1]")
    title(("CL:" + str(np.mean(CL))))
    #xlim(0 ,500)

    subplot(1, 3, 3)
    plot(beta_mol * 1e6 * LRmol, z)
    plot(ext_aer * 1e6, z)
    plot(ext_tot * 1e6, z)
    xlabel("Extinction Coefficient [Mm-1]")
    

print("Done")
# return CL

# %% Save NetCDF

output_file = info['folder_output'] + 'ALC_calibration_' + info['instrument'] + "_" + info["identifier"] + "_" + \
              info["name"] + '.nc'
print(output_file)              
# os.remove(output_file)
if not (os.path.isfile(output_file)):
    print("Creating NetCDF file")

    # create a file (Dataset object, also the root group).
    ncID = Dataset(output_file, 'w', format='NETCDF4')

    # Create Global attibuteclose        
    ncID.station_name = info["name"]
    ncID.instrument_id = instrument_id
    ncID.identifier = info["identifier"]
    ncID.instrument_type = info["instrument_type"]
    ncID.title = "Calibration data for " + info["instrument_type"] + " at " + info["name"]
    ncID.history = " Created by E-PROFILE (" + timing.strftime("%Y-%m-%d %H:%M:%S", timing.gmtime()) + ")"
    ncID.source = "Ground based remote sensing"
    ncID.references = "E-PROFILE calibration description"
    ncID.Conventions = "CF-1.6"

    # dimensions.
    nc_time = ncID.createDimension('time', None)

    # Create variables.
    # You must use zlib=true to preserve an siiiiignificant amount of disk space
    nc_time = ncID.createVariable('time', 'f8', ('time',), zlib=True)
    nc_time.long_name = "Central time (UTC) of the calibration period"
    nc_time.units = "days since 1970-01-01 00:00:00.000"
    nc_time.long_name = "Time (UTC) of the calibration period"
    nc_time.standard_name = "time"

    nc_start_time = ncID.createVariable('start_time', 'f8', ('time',), zlib=True)
    nc_start_time.long_name = "Start time (UTC) of the calibration period"
    nc_start_time.units = "days since 1970-01-01 00:00:00.000"

    nc_end_time = ncID.createVariable('end_time', 'f8', ('time',), zlib=True)
    nc_end_time.long_name = "End time (UTC) of the calibration period"
    nc_end_time.units = "days since 1970-01-01 00:00:00.000"

    nc_lidar_constant = ncID.createVariable('lidar_constant', 'f8', ('time',), zlib=True)
    nc_lidar_constant.long_name = "Value of the lidar constant"
    if info["instrument_type"] == 'Vaisala CL51':
        nc_lidar_constant.units = "V* m^3 / sr"
    elif info["instrument_type"] == 'Vaisala CL31':
        nc_lidar_constant.units = "V* m^3 / sr"
    elif info["instrument_type"] == 'Jenoptik CHM15k':
        nc_lidar_constant.units = "counts/s * m^3 / sr"
    else:
        raise NotImplementedError(" Implement Units for this instrument")

    nc_lidar_constant_uncertainty = ncID.createVariable('lidar_constant_uncertainty', 'f8', ('time',), zlib=True)
    nc_lidar_constant_uncertainty.long_name = "Uncertainties on the lidar constant"
    if info["instrument_type"] == 'Vaisala CL51':
        nc_lidar_constant_uncertainty.units = "V* m^3 / sr"
    elif info["instrument_type"] == 'Vaisala CL31':
        nc_lidar_constant_uncertainty.units = "V* m^3 / sr"
    elif info["instrument_type"] == 'Jenoptik CHM15k':
        nc_lidar_constant_uncertainty.units = "counts/s * m^3 / sr"
    else:
        raise NotImplementedError(" Implement Units for this instrument")

    nc_calibration_bottom_height = ncID.createVariable('calibration_bottom_height', 'f8', ('time',), zlib=True)
    nc_calibration_bottom_height.long_name = "Bottom height of calibration vertical range"
    nc_calibration_bottom_height.units = "m asl"

    nc_calibration_top_height = ncID.createVariable('calibration_top_height', 'f8', ('time',), zlib=True)
    nc_calibration_top_height.long_name = "Top height of calibration vertical range"
    nc_calibration_top_height.units = "m asl"

    nc_calibration_method = ncID.createVariable('calibration_method', 'u1', ('time',), zlib=True)
    nc_calibration_method.long_name = "Method of calibration"
    nc_calibration_method.flag_values = 0, 1, 2, 3
    nc_calibration_method.flag_meanings = "Rayleigh Liquid_waper_clouds Ground_based_lidar Satellite_lidar"

    nc_laser_life_time = ncID.createVariable('laser_life_time', 'f8', ('time',), zlib=True)
    nc_laser_life_time.long_name = "Average laser life time during the calibration"
    nc_laser_life_time.units = "counts/shots"

    nc_laser_wavelength = ncID.createVariable('laser_wavelength', 'f8', ('time',), zlib=True)
    nc_laser_wavelength.long_name = "Wavelength of Laser for channel 0"
    nc_laser_wavelength.units = "nm"

    nc_quality_flag = ncID.createVariable('quality_flag', 'i1', ('time',), zlib=True)
    nc_quality_flag.long_name = "Quality flag"
    nc_quality_flag.flag_values = 0
    nc_quality_flag.flag_meanings = "To_be_defined"

    nc_status_detector = ncID.createVariable('status_detector', 'f8', ('time',), zlib=True)
    nc_status_detector.long_name = "Average detector status during the calibration"
    nc_status_detector.units = "%"
    nc_status_detector.comments = "Corresponds to: Lufft: state_detector"

    nc_status_laser = ncID.createVariable('status_laser', 'f8', ('time',), zlib=True)
    nc_status_laser.long_name = "Average laser energy during the calibration."
    nc_status_laser.units = "%"
    nc_status_laser.comments = "Corresponds to: Lufft: state_laser"

    nc_temperature_laser = ncID.createVariable('temperature_laser', 'f8', ('time',), zlib=True)
    nc_temperature_laser.long_name = "Average laser temperature during the calibration."
    nc_temperature_laser.units = "degrees Celcius"
    nc_temperature_laser.comments = "Corresponds to: Vaisala: Laser temperature"

    nc_temperature_optical_module = ncID.createVariable('temperature_optical_module', 'f8', ('time',), zlib=True)
    nc_temperature_optical_module.long_name = "Average optical module temperature during the calibration."
    nc_temperature_optical_module.units = "degrees Celcius"
    nc_temperature_optical_module.comments = "Corresponds to: Lufft:temp_lom"

    nc_window_transmission = ncID.createVariable('window_transmission', 'f8', ('time',), zlib=True)
    nc_window_transmission.long_name = "Average optical windows transmission during the calibration."
    nc_window_transmission.units = "%"
    nc_window_transmission.comments = "Corresponds to: Vaisala: Windows transmission estimate, Lufft: state_optics"

    # Add  Time and Calibration data

    nc_time[0] = date_nb

    nc_start_time[0] = min(time)
    nc_end_time[0] = max(time)
    nc_lidar_constant[0] = np.median(CL)
    nc_lidar_constant_uncertainty[0] = CL_uncertainties

    # Add Calibration parameter
    nc_calibration_bottom_height[0] = zi
    nc_calibration_top_height[0] = ze
    nc_calibration_method[0] = 0
    nc_quality_flag[0] = flag

    # Add house keeping parameters
    nc_laser_life_time[0] = np.mean(life_time)
    nc_laser_wavelength[0] = wavelength * 1e9
    nc_status_detector[0] = np.mean(state_detector)
    nc_status_laser[0] = np.mean(state_laser)
    nc_temperature_laser[0] = np.nan
    nc_temperature_optical_module[0] = np.mean(temp_lom)
    nc_window_transmission[0] = np.mean(window_transmission)


else:
    print("Updating NetCDF file")
    # Open file
    ncID = Dataset(output_file, 'a')
    # Find if Calibration already exist and overwite it (if same type of calibration)
    if any(date_nb_epoch == ncID.variables['time'][:]):
        index_nc = np.where(date_nb_epoch == ncID.variables['time'][:])
        if len(index_nc[0]) > 1:
            print("Warning: 2 Calibration for this date, take into account only the first one")

            index_nc = index_nc[0][0]

        if ncID.variables["calibration_method"][index_nc] == 1:
            print(" Overwriting existing calibration")
        else:
            index_nc = ncID.variables['start_time'].size
            print("Another type of Calibration already exist for this day, write data at the end of the file")

    else:
        index_nc = ncID.variables['start_time'].size

    # Update Calibration data
    ncID.variables['time'][index_nc] = date_nb_epoch
    ncID.variables['start_time'][index_nc] = min(time)
    ncID.variables['end_time'][index_nc] = max(time)
    ncID.variables['lidar_constant'][index_nc] = np.median(CL)
    ncID.variables['lidar_constant_uncertainty'][index_nc] = CL_uncertainties
    ncID.variables['calibration_bottom_height'][index_nc] = zi
    ncID.variables['calibration_top_height'][index_nc] = ze

    # Update  anb houskeeping parameters
    ncID.variables['laser_life_time'][index_nc] = np.mean(life_time)
    ncID.variables['status_detector'][index_nc] = np.mean(state_detector)
    ncID.variables['status_laser'][index_nc] = np.mean(state_laser)
    ncID.variables['temperature_laser'][index_nc] = np.nan
    ncID.variables['temperature_optical_module'][index_nc] = np.mean(temp_lom)
    ncID.variables['window_transmission'][index_nc] = np.mean(window_transmission)



    #    nc_start_time=min(time)
# nc_end_time=max(time)
#    nc_lidar_constant=np.median(CL)
#    nc_lidar_constant_uncertainty=np.std(CL)

ncID.close()


# alc_cal_rayleigh(date_str,info,options)
