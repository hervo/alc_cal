# -*- coding: utf-8 -*-
#
#
# Modif 21/12/2015: Better quality check and error estimation
# To be tested on a longer period
#Error estimation to be improved

from __future__ import print_function, division, absolute_import

from ALC_cal_rayleigh_tools_v3_UK import alc_cal_rayleigh
from datetime import timedelta, date
from utils import nans
from matplotlib.pyplot import *
from matplotlib import cm
import numpy as np

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

close('all')

#%%  Inputs """
start_date = date(2016,8,1)
end_date = date(2016, 8, 15)

# Define instrument information
info=[   {    'name': 'LERWICK',
    'instrument':"03005", # WMO Number, ICAO or other
   'free_format':"CHM110109", # Free format used by provider
   'identifier':"C",
    'instrument_type':'Jenoptik CHM15k',
   'folder_ECMWF':"D:/ECMWF/",
    'folder_root':"C:/DATA/Ceilometer/E-PROFILE_L1_FILES/", 
    'folder_output':"C:/DATA/Ceilometer/E-PROFILE_L1_FILES/Calibration/",
    'lat':60.133,
    'lon':-1.183,
    }
    ]



# Define Rayleigh calibration options
options={
    'hour_min': 20, # Hour to start calibration on previous day (Should be 21 or 20)
    'hour_max': 4, # Hour to finish calibration (Should be 03 or 04)
    'min_time_range':1, # Minimum hours where you have non cloudy data (Default 3)
    'plot_main': 0,
    'plot_all':  0,
    'z_low_cloud':4000,
    'max_ratio_cloudy':0.5, # Max ratio of cloudy profile (0 to 1)
    'LRaer':52,
    'z_start_ext':100,
    'calc_ext_above_molecular':0,
    'subtract_background':0, # Should be zero !!
    'consider_points_lower_than_molecular':1,
    'threshold_quality':50, # Default 15
    }


# Flag meanings    
flag_list={ '1':'Successfull',
    '0.5':'is_partially_clear_night',
    '-1':'Not a clear night',
    '0':'No data',
    '-2':'Signal is not proportional to Molecular scattering (Rayleigh fit give different results compared to Median Sl/Pmol)',
    '-3':'Other quality ratio',
    '-4':'Missing Model data',
    '-5':'RCS contains only NaN',
    '-6':'Uncertainties higher than CL Values',
    '-7': 'Negative Rayleigh fit',
    '-8': 'Rayleigh fit issue: a is Larger than b'}    


#%% Loop on station
#calc length of date range for preallocation 
N=1
days=[]
for single_date in daterange(start_date, end_date):
    N=N+1
days=[x for x in range(0,N)]
time=[x for x in daterange(start_date, end_date)]
CL=nans((len(info),N))
flag=nans((len(info),N))
Error_CL = nans((len(info),N))

for s in range(0,len(info)):
    print('------------------------------   ' + info[s]['name'] + '  ---------------------------------' )
    i=0
    for single_date in daterange(start_date, end_date):
        date_str= single_date.strftime("%Y%m%d")
        print('-------------------------   ' +date_str+ '  -----------------------------' )
        CL[s,i], flag[s,i],Error_CL[s,i]=alc_cal_rayleigh(date_str,info[s],options)      
        i=i+1
CL[CL==-1]=np.nan
Error_CL[Error_CL==-1]=np.nan
#%% Plot outputs 


figure(figsize=(15,15))

ax1=subplot(2,1,1)
xlim(0,N+10) 
grid('on')

ax2=subplot(2,1,2)
xlim(0,N+10) 
grid('on')
xlabel('Days since ' + start_date.strftime("%d-%m-%Y"))

idx = np.linspace(0, 1, len(info))
colorvec=cm.jet(idx)
width=0.1;

for s in range(0,len(info)):
    days2=[x+width*s/2 for x in range(0,N)]
    if not(np.all(np.isnan(CL))):
        ax1.errorbar(days,CL[s,:],yerr=Error_CL[s,:]*CL[s,:]/100 , label=info[s]['name'],color=colorvec[s,:], fmt='-s')
    
    ax2.bar(days2,flag[s,:],width=width, label=info[s]['name'],color=colorvec[s,:], edgecolor = "none")
    

ax1.legend(loc = 'center right')   
ax2.legend(loc = 'center right') 
#if not(np.all(np.isnan(CL))):
#    ax1.set_yscale('log')
# set flag labels
flag_number=(-8,-7,-6,-5,-4,-3,-2,-1,0,0.5,1)   
flag_meaning=('Rayleigh fit issue: a is Larger than b','Negative Rayleigh fit','Uncertainties higher than CL Values','RCS contains only NaN','Missing Model data','Other quality ratio','Bad Quality ratio','Not a clear night','No data','is_partially_clear_night','Successfull')
yticks(flag_number,flag_meaning)


if np.all(np.isnan(CL)):
    raise NotImplementedError("No CL determined")

#%% Create interactive plot
import bokeh.plotting as bk
def rgb_to_hex(rgb):
    return '#%02x%02x%02x' % rgb
    
f=bk.figure( tools="pan,box_zoom,reset,save,hover")
bk.output_file("calibration_output.html", title="calibration output")    

for s in range(0,len(info)):
    color_hex=rgb_to_hex((int(np.round(colorvec[s,0]*255)),int(np.round(colorvec[s,1]*255)),int(np.round(colorvec[s,2]*255))))
    f.circle(days,CL[s,:],color=color_hex,legend=info[s]['name'])
bk.show(f)


