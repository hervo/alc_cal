import os 
from os.path import join 
import iris  # data management and plotting library from met office
import ncUtils # library for reading netCDF files
import matplotlib.pyplot as plt 
from netCDF4 import Dataset 
import numpy as np 
import copy
from scipy.interpolate import interp1d
import sys
import operator
from scipy import stats
##########################################################################################
#Convert files to iris cubes and save in new format

#-----------------------------------------------------------------------------------------
#Location of files to be concatenated - Met Office files are in hourly intervals
def required_data ():
    """
    input: none
    output: str of date and variable names to be extracted from the netcdf files
    Get location of files
    """
    datapath = './' #path to all data
    req_location = 'Eskdalemuir/Vais'#raw_input('Ceilometer Location: ')             
    req_year = '2015'#str(raw_input ('year of file (format YYYY): '))
    req_month = '09' #str(raw_input ('month of file (format MM): '))
    req_day = '01'   #str(raw_input ('day of file (format DD): '))
    req_date =  join( req_year, req_month, req_day)             
    savepath = join(datapath,req_location,req_date)
    return(savepath, req_date, req_location)
#--------------------------------------------------------------------------------------   
def textfile (savepath):
    """
    input: date of day
    output:list of files (ls_out)
    """
    list_of_saved_files = "ls_out"
    command = "ls "+savepath+" > "+join(savepath,list_of_saved_files)
    #print command
    os.system(command)
    
    fn = open(join(savepath,list_of_saved_files))
    lines = [line.strip() for line in fn]
    lines.remove(list_of_saved_files);
    fn.close()
    
    return (lines)
#--------------------------------------------------------------------------------------#    
def make_cube (savepath,lines):
    """
    input:data file list
    output:cubelist
    loop through files to create cubes of variable data
    """
    cube_list = iris.cube.CubeList() #create cube list to write hourly cubes to
    
    # Set up the  geographic (ellipsoidal) coordinate system, defined by the shape of
    # the Earth and a prime meridian.   To use later with iris when settin lat and lon.
    geog_cs = iris.coord_systems.GeogCS(semi_major_axis=6378137, inverse_flattening=298.257223563)
    
    for whichfile in lines:
        filename1=join(savepath,whichfile)
        print "File to open and read ",filename1
        list_of_contents=ncUtils.nc_listatt(filename1)
        print whichfile+' Attributes : ', list_of_contents
        
        #Assign meta data in the netcdf file to variables
        system =  ncUtils.nc_getatt(filename1, 'system')
        WMO_id =  ncUtils.nc_getatt(filename1, 'WMO_id')
        dday = ncUtils.nc_getatt(filename1, 'day')
        mmonth = ncUtils.nc_getatt(filename1, 'month')
        yyear = ncUtils.nc_getatt(filename1, 'year')
        ddate =  dday+'/'+mmonth+'/'+yyear
        file_title =  ncUtils.nc_getatt(filename1, 'title')
        location=  ncUtils.nc_getatt(filename1, 'location')
        stationID= WMO_id+'_'+system
               
        list_variables=ncUtils.nc_listvar(filename1)
    
        lat = ncUtils.nc_getvar(filename1, 'latitude')
        lon = ncUtils.nc_getvar(filename1, 'longitude')
    
        obsfile=Dataset(filename1)
        time = obsfile.variables['time']
        time_points = obsfile.variables['time'][:]
    
        range = obsfile.variables['range']
        range_points = obsfile.variables['range'][:]
    
        # CBH = Cloud Base Height 
        CBH = obsfile.variables['CBH'][:]
        CBHunit='km'
    
        beta = obsfile.variables['beta']
        beta_points = obsfile.variables['beta'][:]
        
        pulse_energy = obsfile.variables['pulse_energy'][:]

        
        window_tau = obsfile.variables['window_tau'][:]
        
        # Uncomment to return list attributes for each hourly file        
        #for att in list_of_contents: # Loop over Attributes in the netCDF file
            #print att,' : ', ncUtils.nc_getatt(filename1, att)
            
    # Create Iris cube called "beta_cube" to hold the netcdf data with coordinate system (backscatter and all meta-data)
        #  http://scitools.org.uk/iris/docs/latest/index.html
        
        b_time_coord=iris.coords.DimCoord(time_points,standard_name=time.standard_name, long_name=time.long_name, var_name='time', units=time.units)       
        b_range_coord=iris.coords.DimCoord(range_points,long_name=range.long_name, var_name='range', units=range.units)
                                    
        beta_cube=iris.cube.Cube(beta_points,long_name=beta.long_name,units='m-1 sr-1',dim_coords_and_dims=[(b_time_coord, 0), (b_range_coord, 1)])
        
       
        New_coord = iris.coords.AuxCoord(pulse_energy[:],long_name = 'Laser pulse energy, % of nominal factory setting', units = '%')
        beta_cube.add_aux_coord(New_coord, (0,))
        
        New_coord2 = iris.coords.AuxCoord(window_tau[:],long_name = 'window_tau, % unobscured', units = '%')
        beta_cube.add_aux_coord(New_coord2, (0,))
                
        beta_cube.add_aux_coord(iris.coords.AuxCoord(float(lat),
                                                                standard_name='latitude', 
                                                                coord_system=geog_cs)
                                           )
        beta_cube.add_aux_coord(iris.coords.AuxCoord(float(lon),
                                                                standard_name='longitude', 
                                                                coord_system=geog_cs)
                                                )
                
        beta_cube.add_aux_coord(iris.coords.AuxCoord(str(WMO_id),long_name='station_wmo_id')        
                                                )                        
                                                
        beta_cube.add_aux_coord(iris.coords.AuxCoord(str(file_title)))
        
                                                               
        beta_cube.add_aux_coord(iris.coords.AuxCoord(str(location),
                                                                long_name='site_name')
                                                )
                                                
        beta_cube.add_aux_coord(iris.coords.AuxCoord(str(stationID),
                                                                long_name='site_longname')
                                                )
                
        print 'shape CBH ',type(np.ma.shape(CBH)),np.ma.shape(CBH)
    
        for i, name in enumerate(['lower', 'middle', 'upper']):
            coord = iris.coords.AuxCoord(CBH[:, i], units=CBHunit,
                                         long_name='cbh_{}'.format(name))
            beta_cube.add_aux_coord(coord, (0,))
             
       ####### 
        print '@@@@@@@ Backscatter Cube @@@@@@@'
        #print beta_cube
        #print '@@@@@@====== ',beta_cube.coord('site_name').points,type(beta_cube.coord('site_name').points)
        location_name=str(beta_cube.coord('site_name').points[0])
        #print location_name, type(location_name)
        
        # Create a matrix of values with a mask. Mask out the noisy data (backscatter less than 1e-12)
        beta_cube_mask = np.ma.masked_less(beta_cube.data, 1e-12)
        
	cube_list.append(beta_cube)        
    return(cube_list)
#------------------------------------------------------------------------------------#    
def day_cube (cube_list):
    """
    input:cube_list
    output: single cube for one day
    concatenate hourly files
    """
    daycube = cube_list.concatenate()[0]        
    return (daycube)
#------------------------------------------------------------------------------------#    
def save_cube(daycube, the_site, the_date):
    """
    input: daycube
    output: cube of required variable for one day saved
    """
    filename = './'
    the_date = the_date + '.nc'
    file_tosave = join(filename, the_site, the_date)
    directory = file_tosave[:-5]
    system("mkdir -p "+directory) 
    print 'SAVE TO ', file_tosave
    iris.fileformats.netcdf.save(daycube, file_tosave, netcdf_format='NETCDF4')
    return ()
    
##########################################################################################
#Load in Daily Cubes

#------------------------------------------------------------------------------------# 
#List of files to open
def ListToOpen(path,Location, Instrument, Year, Month):
    """
    Create list of files to open
    Input: Location, Instrument, Year, Month
    eg ('Eskdalemuir', 'Jen' , '2014', '09')
    Output: Array of filenames
    """

    #path to list of files
    #path = '/home/qt013194/Project_Calibration/data/'
    filepath = join(str(path), str(Location), str(Instrument), str(Year), str(Month))
    #create file with list of files
    list_of_saved_files = "ls_out"
    command = "ls " + filepath+" > " + join(filepath,list_of_saved_files)
    print 'CHECK ListToOpen: ', command
    os.system(command)
    print type(filepath), filepath
    if os.path.isdir(filepath):
        #loop through to import into array
        fn = open(join(filepath,list_of_saved_files))
        lines = [line.strip() for line in fn]
        lines.remove(list_of_saved_files);
        fn.close()
    else:
        print 'ERROR: Directory does not exist'
        lines = []   
    return (lines)
#------------------------------------------------------------------------------------#     
def opencube(filename):
    """
    Open file
    """
    daycube_aslist = iris.load(filename)
    daycube = daycube_aslist[0]
    return (daycube)    
#------------------------------------------------------------------------------------# 
def data(daycube):
    #extract beta, time, range
    beta_data = np.ma.masked_less(daycube.data, 1e-12)
    time_data = daycube.dim_coords[0]
    range_data = daycube.dim_coords[1]
    return (beta_data, time_data, range_data) 
##########################################################################################
#Scatter Corrections

#------------------------------------------------------------------------------------#
#Note that this will apply to clear sky profiles as well but this will be filtered out later so this is not an issue
def find_cloud(beta_data):
    """
    Find the location of the max beta
    Take cloud range as 15 gates above and below
    return array with 30gate range beta only; others set to nan
    """
    arr = np.copy(beta_data)
    for i in xrange (len(np.transpose(arr))):
       

        index, value = max(enumerate(arr[:,i]), key=operator.itemgetter(1))
        loc_above = index + 15 
        loc_below = index - 15
        arr[loc_above:,i] = np.nan #set everything above to nan
        arr[:loc_below,i] = np.nan #set everything below to nan
    return(arr)

#------------------------------------------------------------------------------------#
#Only applied upto about 2.4km as cloud above this will be discarded in calibration anyway
#Corrections for above this available from source
def scatter_correct_Vais(cloud_beta, range_data):
    """
    Input: raw beta data
    Output: beta with scattering correxction applied
    Apply multiple scattering correction - source:  http://www.met.reading.ac.uk/~swr99ejo/lidar_calibration/index.html
    """
    Scat_correct_b = np.copy(cloud_beta)
    range_km = range_data.points    
    #Apply height dependent eta to beta values - i.e. multiply out multiple scattering
    #Note these values are instrument dependent
    ind1 = np.where(range_km< 0.250)
    Scat_correct_b[ind1,:]=Scat_correct_b[ind1,:]* 0.82881
    ind2 = np.where((np.abs(range_km - 0.375)) < 0.125)
    Scat_correct_b[ind2,:]=Scat_correct_b[ind2,:]*0.82445
    ind3 = np.where((np.abs(range_km - 0.625)) < 0.125)
    Scat_correct_b[ind3,:]=Scat_correct_b[ind3,:]*0.81752
    ind4 = np.where((np.abs(range_km - 0.875)) < 0.125)
    Scat_correct_b[ind4,:]=Scat_correct_b[ind4,:]*0.81021
    ind5 = np.where((np.abs(range_km - 1.125)) < 0.125)
    Scat_correct_b[ind5,:]=Scat_correct_b[ind5,:]*0.80241	
    ind6 = np.where((np.abs(range_km - 1.375)) < 0.125)
    Scat_correct_b[ind6,:]=Scat_correct_b[ind6,:]*0.79356
    ind7 = np.where((np.abs(range_km - 1.625)) < 0.125)
    Scat_correct_b[ind7,:]=Scat_correct_b[ind7,:]*0.78595
    ind8 = np.where((np.abs(range_km - 1.875)) < 0.125)
    Scat_correct_b[ind8,:]=Scat_correct_b[ind8,:]*0.77877 
    return (Scat_correct_b)    
#------------------------------------------------------------------------------------#
#Merge scattering correction with data in beta cube
def corr_beta (Scat_beta, beta_data):
    """
    Locate nans placed by finding cloud(see above)
    replace with beta values
    s_cor_beta is array of beta with scatting correction applied to 30 gates around max beta
    """
    s_cor_beta = np.copy(Scat_beta)

    for prof in  xrange (len(np.transpose(s_cor_beta))):
        index_n = np.isnan(Scat_beta[:,prof])
        thenans = np.where(index_n == True)
        for locnan in xrange(len(thenans)):
           s_cor_beta[(thenans[locnan]), prof] = beta_data [(thenans[locnan]), prof]
       
       
    return(s_cor_beta)  
    
    
##########################################################################################    
#Calclate and filters for S

#------------------------------------------------------------------------------------#   
def lidar_ratio(Scat_correct_b,range_data, Instrument):
    """
    Input:scatter corrected beta data (attenuated backscatter coefficient)
    Output: lidar ratio (ratio backscatter coefficient to extinction coefficient)
    """
    # Gate size dependent on instrument
    # Gate2400 marks top of integration - must be 2.4km for H2_off instruments (currently true for Met Office). Could be increased for others
    #begin marks start of integration - avoid problematic gates and worst of overlap in CL51s
    if Instrument == 'Vais':
        Gate2400 = 120
        gatesize = 20
        begin = 10
    elif Instrument == 'CL51':
        Gate2400 = 240
        gatesize = 10
        begin = 20
    elif Instrument == 'SIRTA':
        Gate2400 = 160
        gatesize = 15
        begin = 13 #~200m        
    else:
        print 'Incorrect Instrument Input'    
    # Integrated from 0.2 - 2400km 
    inc_beta = Scat_correct_b[begin:Gate2400,:]      #betas between 0.2 - 2400km
    integrate_S = ((sum(inc_beta))*gatesize)
    S = ((integrate_S*2)**-1)
    
    return(S)    
#------------------------------------------------------------------------------------#  
###########################################################################
#           ######   Attenuated Beta Filters   ##########
###########################################################################
#include only profiles where beta 300m above max is at least 20 times smaller
def filter_300 (beta_data, Instrument ):
    """
    Hogan et al (2003b); O'Connor et al (2004) - to be a thick highly attenuating cloud, peak att_Beta must be a factor of 20 times greater than 300m above
    Instrument dependent - different gate sizes
    Where the condition is not passed, value is replaced with a NaN
    """
    # Gate size dependent on instrument
    # Ensure cloud is above 100m
    if Instrument == 'Vais':
        Gate300m = 15
    elif Instrument == 'CL51':
        Gate300m = 30   
    elif Instrument == 'SIRTA':
        Gate300m = 20     
    else:
        print 'Incorrect Instrument Input'
        
    beta_300f = np.copy(beta_data)
    a = np.zeros(Gate300m)
    a = np.transpose(a)
    ex2 = 0
    import operator
    
    for i in xrange (len(np.transpose(beta_300f))):
        profs_sort = np.append(beta_300f[:,i],a)

        index, value = max(enumerate(beta_300f[:,i]), key=operator.itemgetter(1))
        loc_compare_p = index + Gate300m
        compare_p = profs_sort[(loc_compare_p)]
        
        if loc_compare_p < 0 or compare_p*20 > value: 
            beta_300f[:,i] = np.nan
            ex2 = ex2 +1
        else:
            pass

    print'300m above filtered out: ', ex2 
    
    return (beta_300f)
#------------------------------------------------------------------------#
#include only profiles where beta 300m below max is at least 20 times smaller
def filter_300below (beta_data,Instrument):
    """
    Follows reasoning of 300m above filtering
    Instrument dependent - different gate sizes
    Where the condition is not passed, value is replaced with a NaN
    """
    # Gate size dependent on instrument
    # Ensure cloud is above 100m
    if Instrument == 'Vais':
        Gate300m = 15 
    elif Instrument == 'CL51':
        Gate300m = 30
    elif Instrument == 'SIRTA':
        Gate300m = 20          
    else:
        print 'Incorrect Instrument Input'
        
    beta_300belf = np.copy(beta_data)
    #add on 300m of zeros so bottom of profile isnt compared to top
    a = np.zeros(Gate300m)
    a = np.transpose(a)
    ex2 = 0
    import operator
    
    for i in xrange (len(np.transpose(beta_300belf))):
        profs_sort = np.append(beta_300belf[:,i],a)

        index, value = max(enumerate(beta_300belf[:,i]), key=operator.itemgetter(1))
        loc_compare_p = index - Gate300m
        compare_p = profs_sort[(loc_compare_p)]
        
        if loc_compare_p < 0 or compare_p*20 > value: 
            beta_300belf[:,i] = np.nan
            ex2 = ex2 +1
        else:
            pass
    print '300m below filtered out: ', ex2 
    
    return (beta_300belf)    
     
#---------------------------------------------------------------------------------------#
def total_beta(beta_data, Instrument):
    """
    Input: scatter corrected beta data from cube, (between selected indices??)
    Output: integrated beta for each 30s profile
    """
    # Gate size dependent on instrument
    if Instrument == 'Vais':
        Gate2400 = 120
        gatesize = 20
    elif Instrument == 'CL51':
        Gate2400 = 240
        gatesize = 10 
    elif Instrument == 'SIRTA':
        Gate2400 = 160
        gatesize = 15           
    else:
        print 'Incorrect Instrument Input'    
    
    inc_beta = beta_data[5:Gate2400,:]   #betas above first 5 gates (near view issues)
    integrate_beta = ((sum(inc_beta))*gatesize)
    return (integrate_beta)


def inteB_belowCBH(beta_data,range_data, Instrument):
    """
    Integrate beta up to 100m below max beta 
    Includes extra 100m at bottom of profile as this is the region where aerosol is expected
    """
    # Gate size dependent on instrument
    if Instrument == 'Vais':
        G100m = 5
        gatesize = 20
    elif Instrument == 'CL51':
        G100m = 10
        gatesize = 10     
    elif Instrument == 'SIRTA':
        G100m = 7
        gatesize = 15             
    else:
        print 'Incorrect Instrument Input' 
    # Sum beta_data values of profile
    inte_beta = []
    a = len(np.transpose(beta_data))
    for prof in xrange(a):
        loc_belowCBH = np.where(beta_data[:,prof] == np.max(beta_data[:,prof]))[0][0]
        loc = loc_belowCBH - G100m #100m below

        inc_beta = beta_data[5:loc,prof]      #betas between gate 5 - 100m below
        integrate_beta = ((sum(inc_beta))*gatesize)
        inte_beta.append(integrate_beta)
    inte_beta[inte_beta==0] = np.nan
    return(inte_beta)
    

def filter_ratio(beta_data,range_data, ratio_val, Instrument):
    """
    Hogan et al (2003b); O'Connor et al (2004)    - exclude strong background aerosol events 
    Input: raw beta data, set required ratio
    integrate over full profile values and integrate up to 100m below CBH
    If beta below CBH represents more than 5% of total, profile discarded
    Output: beta with nans for profiles that don't qualify
    """
    integrated_B = total_beta(beta_data, Instrument)
    integrated_belCBH_B = inteB_belowCBH(beta_data,range_data, Instrument)
    B_ratio = integrated_belCBH_B/integrated_B
    ratiof_B = np.copy(beta_data)
    filt_out = 0
    #eliminate profiles where beta below CBH represents more than 5% of total
    for i in range(len(B_ratio)):
        if B_ratio[i] > ratio_val:
            ratiof_B[:,i] = np.nan
            filt_out = filt_out +1
        elif B_ratio[i] < 0:
            ratiof_B[:,i] = np.nan 
            filt_out = filt_out +1       
        else:
            pass 
    print 'ratio filtered out: ', filt_out   
    return(ratiof_B, B_ratio)
#--------------------------------------------------------------------------------------#   
############### Bring att_beta filtering together######################
def locate_nan(arr):
    """
    Input: version of beta with nans created by a filter
    Output: location of nans in beta array
    """
    index_n = np.isnan(arr)                 #e.g. beta_300f
    find_n = np.where(index_n == True)
    loc_n1 = find_n[1]
    loc_n2 = np.squeeze(loc_n1)
    loc_n = np.unique(loc_n2)
    return (loc_n)
#------------------------------------------------------------------------------------#     
def filter_bynan (loc_n,S):
    """
    Input: Output for locate_nan() and second array to filter (usually S)
    Output: Filtered version of e.g. S (nans in place)
    """
    print np.shape(S)
    filt_S = np.copy(S)
    for loc in range(len(loc_n)):
        filt_S[(loc_n[loc])] = np.nan
    return (filt_S)
#------------------------------------------------------------------------------------# 
def step1_filter(beta_data, range_data,ratio_val,S, Instrument):
    """
    Using scatter corrected data, run through several filters to remove profiles unsuitable for calibration
    filter1: aerosol from 100m below CBH (max B) to gate5 must not be more than (e.g).5% of total integrated B (0.2-2.4km)    
    filter2: B 300m above max B must be 20x smaller
    filter3: B 300m below max B must be 20x smaller 
    NB. filter functions above, called by this one
    beta data with nans for profiles that did not pass the filters
    OUTPUT: S with nan where filters not passed
    """
    filter1, B_ratio = filter_ratio(beta_data, range_data, ratio_val, Instrument)    
    filter2 = filter_300 (filter1, Instrument)
    filter3 = filter_300below(filter2, Instrument)     
       
    loc_beta_f = locate_nan(filter3)
    filtered_S = filter_bynan(loc_beta_f, S)
    return(filtered_S)
    
###########################################################################
#               ######   Lidar Ratio Filter   ##########
###########################################################################
def step2_Sfilt (S, S_range_percent):
    """
    loop through lidar ratio only keeping if within 10% (or other) of
    value before and after
    """
    plus = 100. + S_range_percent
    minus = 100. - S_range_percent
    const_S = np.zeros(len(S))
       
    for i in range(len(S)-2):
        if (S[i+1]/S[i]*100 <= plus and S[i+1]/S[i]*100 >= minus and S[i+1]/S[i+2]*100 <= plus and S[i+1]/S[i+2]*100 >= minus):                
            const_S[i+1] = S[i+1]
        else:
            const_S[i+1] = np.nan
    const_S[-1] = np.nan
    const_S[0] = np.nan             ##loop above necessitates neighbouring profiles so first and last index not checked - therefore set to nan   
    return(const_S)
    
######################################################################################
#calculate stats summaries
#------------------------------------------------------------------------------------#
import numpy as np
def Plot_ashist(variable):
    v = np.copy(variable)
    v[np.isnan(v)] = 0
    vartoplot = v[np.nonzero(v)]
    if len(vartoplot) > 1:
        b = (np.round(np.max(vartoplot)))
        
        #plt.figure(figsize = (6,4))  #uncomment to see plot
        counts, bins, range = plt.hist(vartoplot, bins = (2*b), range = (0,b))
        #plt.xticks(np.arange(0, 110, 10.0), fontsize = 18)
        plt.yticks(fontsize = 18)
        #plt.axis(xmax = b+1, xmin = 16)
        bin_centers = 0.5 * np.diff(bins) + bins[:-1]
        
        plt.title('Calculated Apparent Lidar Ratio',fontsize = 20)
        plt.xlabel('Apparent S [sr]', fontsize = 18)
        plt.ylabel('Frequency', fontsize = 18)
        plt.tight_layout()
        
        #plt.axis(xmax = 100)
        #plt.show()
    else:
        counts = 0    
    return(counts)        
#------------------------------------------------------------------------------------# 
def S_Stats (Step2_S, Cal_hist):
    """
    Calculate daily mode for S
    if number of profiles in mean is less than 20 (10 mins), daily stats will not be used in the calibration (ie are set to nan) - this is derived from Cal_hist
    Cal_hist gives histogram values so max gives the mode
    """
    var = np.copy(Step2_S)
    peak = np.max(Cal_hist)
    if peak > 10.:          #must have at least 10 profiles in filtered S
        print '@@@@@@@@', peak, '@@@@@@@'
        m = np.asarray(var)
        
        m[np.isnan(m)]= 0
        m = m[np.nonzero(m)]
        mean = np.mean(m)              
        print 'calibration mean = ', mean
        
        sem = stats.sem(m)
        print 'std error = ', sem
        
        m2 = np.round((m.tolist()),1)
        mode_arr = stats.mode(m2)
        mode = mode_arr[0][0]
        print 'calibration mode = ', mode
        
        median = np.median(m)
        print 'calibration median = ', median
    else:
        print '######', peak, '#####'
        mode = np.nan
        mean = np.nan
        median = np.nan
        sem = np.nan
    return (mode, mean, median, sem)   
