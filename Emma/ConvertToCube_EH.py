"""
Emma Hopkin, University of Reading
November 2015
Cloud Calibration of Vaisala Ceilometers
Based on O'Connor et al. (2004)
"""

import os 
from os.path import join 
import iris  # data management and plotting library from met office
import ncUtils # library for reading netCDF files
import matplotlib.pyplot as plt 
from netCDF4 import Dataset 
import numpy as np 
import copy
from scipy.interpolate import interp1d
import sys
import VaisCeil_Calibration_utils_EH as EH_utils
####
#Convert netCDF files to iris cubes
def main():
    # Set up standard levels for the logarithmic plotting of backscatter
    std_beta_levels=np.linspace(-7,-3, 8)
    beta_levels_option1=np.linspace(-9.0,-3.2, 10)
  
    #Sort required info
    savepath,req_date, req_location = EH_utils.required_data()
    print '@@@', savepath
    #Create textfile of data files to be used
    lines = EH_utils.textfile(savepath)
    #create beta cube - concatenate into one day
    cube_list = EH_utils.make_cube(savepath,lines)
    daycube = EH_utils.day_cube(cube_list)
    print daycube
    #save beta cube
    EH_utils.save_cube(daycube, req_location, req_date)
    return()
      
    
if __name__ == "__main__":
    main()    
    
