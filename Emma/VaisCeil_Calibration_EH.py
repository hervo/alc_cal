"""
Emma Hopkin, University of Reading
November 2015
Cloud Calibration of Vaisala Ceilometers
Based on O'Connor et al. (2004)
"""

import os 
from os.path import join 
import iris  # data management and plotting library from met office
import ncUtils # library for reading netCDF files
import matplotlib.pyplot as plt 
from netCDF4 import Dataset 
import numpy as np 
import copy
from scipy.interpolate import interp1d
import sys
import VaisCeil_Calibration_utils_EH as EH_utils
import operator
##############################################################################################
path = 'C:/DATA/Ceilometer/CL31/UKMET/' #Path to all Cubes
Location = 'MiddleWallop' #Ceilometer site directory
Instrument = 'Vais'     #Instrument directory
Year = ['2014']#,'2015']  #Year directories to loop through
Month = ['10']#['01','02','03','04','05','06','07','08','09', '10', '11', '12']#Month directories to loop through

ratio_filt = 0.05 

#empty lists to fill
data_dates = []
modes = []
means = []
medians = []
sems = []
window_tau = []
window_tau_alert=[]
pulse_energy = []
pulse_energy_alert = []
CBH = []
All_S = []
maxB = []
maxB_range = []

for year in Year:
    for month in Month:
        ##list of daily files in the month 
        filelist = EH_utils.ListToOpen(path, Location, Instrument, year, month)


        for ll in filelist:
            str_date = year+month+ll[:-3]
            dates = join(year, month, ll[:-3])
            data_dates.append(dates)
            ##For each daily file:
            print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~' 
            filepath = join(path, Location, Instrument, year, month, ll)
            daycube = EH_utils.opencube(filepath)
            beta, time_data, range_data = EH_utils.data(daycube)
            beta_data = np.transpose(beta.data)
            ##Apply scattering correction
            cloud_beta = EH_utils.find_cloud(beta_data)
            Scat_correct_b = EH_utils.scatter_correct_Vais(cloud_beta, range_data)
            beta_arr = EH_utils.corr_beta (Scat_correct_b, beta_data)
            
            S = EH_utils.lidar_ratio(beta_arr,range_data, Instrument)
            
            ##Apply beta filters
            
            Step1_S = EH_utils.step1_filter(beta_data, range_data,ratio_filt,S, Instrument)  
            ##Apply S Filters
            Step2_S = EH_utils.step2_Sfilt (Step1_S, 10)            #range in S = 10%
            Step2_S[Step2_S <0] = np.nan #(remove neg values caused by neg noise)
            print 'DATE: ',year, month,ll
            All_S = np.concatenate((All_S,Step2_S))
            
            ##Extract CBH_lower
            CBH_data = daycube.coord('cbh_lower')
            CBH = np.concatenate((CBH,CBH_data.points)) 
            
            ##Calculate stats for the day
            Cal_hist = EH_utils.Plot_ashist(Step2_S) #Histogram of filtered S
            day_mode, day_mean, day_median, day_sem = EH_utils.S_Stats(Step2_S, Cal_hist)           

            modes.append(day_mode)
            means.append(day_mean)
            medians.append(day_median)
            sems.append(day_sem)   
            
            ####Additional Queries - check pulse energy and window tau####
            if Instrument == 'Vais':
                mask_crit = 90 #(%)
                
                window_tau_prof = daycube.coord('window_tau, % unobscured')
                window_monitor = np.ma.masked_greater(window_tau_prof.points, mask_crit)
                window_obscured = len(np.where(window_monitor.mask==False)[0])
                if window_obscured > 0:
                    window_tau_alert.append(1)
                else:
                    window_tau_alert.append(0)
                window_tau = np.concatenate((window_tau,window_tau_prof.points))
                
                pulse_energy_prof = daycube.coord('Laser pulse energy, % of nominal factory setting')
                pulse_monitor = np.ma.masked_greater(pulse_energy_prof.points, mask_crit)
                pulse_drop = len(np.where(pulse_monitor.mask==False)[0])
                if pulse_drop > 0:
                    pulse_energy_alert.append(1)
                else:
                    pulse_energy_alert.append(0)
                pulse_energy = np.concatenate((pulse_energy,pulse_energy_prof.points))         
            else:
                pass
                
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Print dates with window_tau or pulse_energy alert
loc_w_alert = np.where(np.asarray(window_tau_alert) == 1)[0]
print '~~~Dates where window_tau is less than threshold~~~'
for w in loc_w_alert:
    print data_dates[w]
print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

loc_p_alert = np.where(np.asarray(pulse_energy_alert) == 1)[0]
print '~~~Dates where pulse_energy is less than threshold~~~'
for p in loc_p_alert:
    print data_dates[p]
print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'                          
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  

plt.figure (figsize =(15,6))
plt.title('Daily Calibration Coefficient - '+ Location, fontsize = 20)
plt.ylabel (r'$C_L$',fontsize = 16)
plt.yticks(fontsize = 16)
x = np.arange(0,(len(medians)))
med2 = np.array(medians)
C_L = med2/18.8
plt.plot(x, C_L,'x',markeredgewidth = 2, markersize = 12, c = 'k', label = 'Calibration Coefficient')

cc = np.asarray(C_L)
cc[np.isnan(cc)]= 0
cl = cc[np.nonzero(cc)]
cl_mean = np.mean(cl)
print 'cl_mean = ', cl_mean
cl_std = np.std(cl)
print 'cl_std = ', cl_std

plt.axis(ymin = np.min(cl)-1, ymax =np.max(cl)+1 , xmin = -1, xmax = len(modes)+1)
plt.grid()
plt.show()







  
