import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt

def nc_getatt(file, attname):
    """
    Get a netcdf attribute from a file. Deals with opening and closing of files,
    and also accepts a list of attribute names.
    """
    # Check for str attname type, and convert to list if not
    if type(attname) in (str, unicode):
        attlist = [attname]
    elif type(attname) == list:
        if np.all([type(item) in (str, unicode) for item in attname]):
            attlist = attname
        else:
            raise Exception("Error, input must be of type string or a list of strings."
                            "The list contains non-string values")
    else:
        raise Exception("Error, input must be of type string or a list of strings")

    # Open the netcdf file
    ncfile = Dataset(file)
    returnlist = []

    # Loop over the variables
    for att in attlist:
        exec("returnlist.append(ncfile.%s)" % att)

    # Close file and return the list of attributes, or just the attribute if it is
    # a list of length one.
    ncfile.close()
    if len(returnlist) == 1:
        returnlist = returnlist[0]

    return returnlist

def nc_getvar(file, varname):
    """
    Get a netcdf variable from a file. Deals with opening and closing of files, and
    also accepts a list or variable names.
    """
    # Check for str varname type, and convert to list if not
    if type(varname) in (str, unicode):
        varlist = [varname]
    elif type(varname) == list:
        varlist = varname
    else:
        raise Exception("Error, input must be of type string or list")

    # Open the netcdf file
    ncfile = Dataset(file)
    returnlist = []

    # Loop over the variables
    for var in varlist:
        returnlist.append(ncfile.variables[var][:].squeeze())
        
    # Close file and return the list of variables, or just the variable if it is
    # a list of length one.
    ncfile.close()
    if len(returnlist) == 1:
        returnlist = returnlist[0]

    return returnlist

def nc_listatt(file):
    """
    Returns a list of the variables in a netcdf file. Deals with opening and closing
    the file
    """
    # Open the file
    ncfile = Dataset(file)

    # Get the attribute list
    attlist = ncfile.ncattrs()
    
    # Close the file and return the list
    ncfile.close()
    return attlist

def nc_listvar(file):
    """
    Returns a list of the variables in a netcdf file. Deals with opening and closing
    the file
    """
    # Open the file
    ncfile = Dataset(file)

    # Get the variable list
    varlist = ncfile.variables.keys()
    
    # Close the file and return the list
    ncfile.close()
    return varlist

def nc_searchvar(ncfile, expression, var_att = False, glob_att = False):
    """
    Searches the variables in an active netcdf file for a string expression in the
    names and prints a list of all names that contain a match.

    Matches are case insensitive.

    If var_att is set to true, the expression match is performed on any variable
    attributes that are of type string, as well as variable names.
    """
    # Check ncfile input is correct
    if type(ncfile) != Dataset:
        raise Exception("Error, ncfile must be of type netCDF4.Dataset")

    findlist = []

    # Loop over the variables and check for regular expression
    for key in ncfile.variables.iterkeys():
        if expression.lower() in key.lower():
            findlist += [key]
            continue
        # If desired, also test string attributes for matches
        if var_att:
            for att_name in ncfile.variables[key].ncattrs():
                exec("att = ncfile.variables[key].%s" % att_name)
                if type(att) in [str, unicode]:
                    if expression.lower() in att.lower():
                        findlist += [(key, att)]
                        break

    # If desired, also check all global attributes
    if glob_att:
        for att_name in ncfile.ncattrs():
            exec("att = ncfile.%s" % att_name)
            if expression.lower() in att_name.lower():
                findlist += [("GLOB_ATT",att_name, att)]
    
    # Print the matches
    for item in findlist:
        print item

def nc_datadump(variables, filename, glob_atts=None, dim_names=None):
    """
    Writes the variables in the dictionary to netcdf file. Currently handles
    types numpy.ndarray, lists, strings and dictionarys.
    """
    # Open a netcdf file for writing
    ncfile = Dataset(filename, 'w', format='NETCDF3_CLASSIC')
    
    # Loop over the values in the dictionary
    for varname, var in variables.iteritems():
        __add_nc_variable__(ncfile, varname, var)
        
    # Loop over the glob_atts (if required)
    if glob_atts != None:
        for atrname, atr in glob_atts.iteritems():
            exec("ncfile.%s = atr" % atrname)

    # Rename the dimensions (if required)
    if dim_names != None:
        for dimlength, dimname in dim_names.iteritems():
            ncfile.renameDimension(str(dimlength), dimname)
    
    # Close netcdf file
    ncfile.close()

def __create_nc_array__(ncfile, varname, var, attributes={}):
    """
    Creates a variable
    """
    # Now save the variable
    ncfile.createVariable(str(varname), var.dtype, map(str, var.shape))[:] = var
    
    # Add the attributes
    for attname, att in attributes.iteritems():
        exec("ncfile.variables[varname].%s = att" % attname)

def __create_nc_dims__(ncfile, var):
    """
    Create the dimensions required for the variable. Here we assume the variable
    is of type np.ndarray
    """
    # Loop over the dimensions of var. If there is not already a dimension
    # of that size in the netcdf file, create one
    for dim in var.shape:
        if not(dim in (len(dimval) for dimval in ncfile.dimensions.values())):
            ncfile.createDimension(str(dim),dim)

def __add_nc_variable__(ncfile, varname, variable, attributes={}):
    """
    Given an open netcdf file, this procedure adds a variable and its attributes
    """
    if type(variable) == list:
        # Convert a list to array before making variable
        __create_nc_dims__(ncfile, np.array(variable))
        __create_nc_array__(ncfile, varname, np.array(variable), attributes=attributes)
    elif np.isscalar(variable):
        variable=np.array([variable])
        __create_nc_dims__(ncfile, variable)
        __create_nc_array__(ncfile, varname, variable, attributes=attributes)
    elif type(variable) == np.ndarray:
        # Write variable
        __create_nc_dims__(ncfile, variable)
        __create_nc_array__(ncfile, varname, variable, attributes=attributes)
    elif type(variable) == np.ma.MaskedArray:
        # Convert to array before making variable
        __create_nc_dims__(ncfile, variable)
        __create_nc_array__(ncfile, variable, attributes=attributes)
    elif type(variable) == dict:
        # If dictionary type then take the data attribute and add the rest as attributes
        try:
            myvar = variable.copy()
            var = myvar.pop('data')
        except KeyError:
            raise Exception('Dictionary must have data under key "data"')
        __add_nc_variable__(ncfile, varname, var, myvar)
    else:
        # Otherwise raise error.
        raise Exception("nc_datadump : Variables of type %s not currently supported" %
                        type(variable))

def nc_compare(file1, file2, diff=True, plot=False):
    """
    Gets all the variables in file 1. Then checks for variables of the same name
    in file2 and finds the differences.

    Prints no difference when there is no difference, or plots the differences
    for those where there is, if the array is of dimension 2 or less.

    The diff keyword sets the plot to be a difference. If set to false, this will
    plot the two variables
    """
    # Get a list of the variable names in file1 and file2
    file1varnames = nc_listvar(file1)
    file2varnames = nc_listvar(file2)
    
    # Create a list of all the variables in file1, and a blank list for file2
    file1vars = nc_getvar(file1, file1varnames)
    file2vars = [None]*len(file1vars)
    
    # Loop over the file1varnames and populate file2vars where appropriate
    for i, varname in enumerate(file1varnames):
        if varname in file2varnames:
            # If the varname is in file2, we put it in the array
            file2vars[i] = nc_getvar(file2,varname)
        else:
            # If its not, then we leave the value as None
            pass
        
    # Now we loop over the two variables and where there are matches make a comparison
    for var1, var2, varname in zip(file1vars, file2vars, file1varnames):
        if np.all(var1 == var2):
            print "Variable match : %s " % varname
        else:
            if plot:
                if np.isscalar(var1):
                    print "Variable difference : %s " % varname
                    print "  - %f : %f " % (var1, var2)
                elif len(var1.shape) <= 2:
                    try:
                        if diff:
                            plt.plot(var1-var2)
                        else:
                            plt.plot(var1, label=file1)
                            plt.plot(var2, label=file2)
                            plt.legend().draggable()
                        plt.ylabel(varname)
                        plt.show()
                    except:
                        plt.clf()
                else:
                    print "Variable difference : %s " % varname
                    print "  - Not attempting to plot, large array"
                    print "  - shape : %s " % str(var1.shape)
            else:
                print "Variable difference : %s " % varname 
