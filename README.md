Welcome to the E-PROFILE ALC calibration Code
-----------------------------------------------------------
AUTOMATIC LIDARS and CEILOMETERS calibration algorithm with:

* Molecular signal (Rayleigh Calibration)
* Liquid Cloud Signal (Cloud Calibration) (Not yet operational)

To be implemented: Water vapour absorption correction for instruments emitting in near infrared.

Tested on windows 7, with Python 3.5, Conda  3.14.1, and NetCDF 4 module


# Rayleigh Calibration #
To use the Rayleigh Calibration, define the following variables in *ALC_CAL.py*:

- start_date
- end_date
- info (for each instrument)
- options


For each instrument, A NetCDF file is created or appended. The value of the Calibration constant and a flag are returned for each day. 
The following flags are defined:

- 1 :  Successful
- 0.5:is partially clear night
- 0  :  No data
- -1 : Not a clear night
- -2 : High Quality ratio
- -3 : Other quality ratio

Positive flags indicate successful calibrations. Null or negative flags indicate that no calibration was possible for a specific day.

## Options for Rayleigh calibration ##
    "hour_min": 20, # Hour to start calibration on previous day (Should be 21 or 20)
    "hour_max": 4, # Hour to finish calibration (Should be 03 or 04)
    "min_time_range" :3, # Minimum hours where you have non cloudy data (Default 3)
    "plot_main": 0,
    "plot_all":  0,
    "z_low_cloud":4000,
    "max_ratio_cloudy":0.5, # Max ratio of cloudy profile (0 to 1)
    "LRaer":52,
    "z_start_ext":100,
    "calc_ext_above_molecular":0,
    "subtract_background":0, # Should be zero !!
    "consider_points_lower_than_molecular":1,
    "threshold_quality":15, # Default 15
    "use_std_atm":1, # Use Standard ATM instead of ECMWF T and P profiles
    "folder_ECMWF":"E:/ECMWF/",
    "folder_root":"/data/pay/REM/ACQ/E_PROFILE_ALC/L1_FILES/", 
    "folder_output":"/data/pay/REM/ACQ/E_PROFILE_ALC/L1_FILES/Calibration/"
    


# ECMWF Data #
**You can use Standard Pressure and temperature instead of ECMWF data if you use the option `use_std_atm:1`** 

For Rayleigh Calibration, Pressure and temperature profiles are useful.

This code is made to Run with ECMWF dataset ([http://apps.ecmwf.int/datasets/data/interim-full-daily/?levtype=pl](http://apps.ecmwf.int/datasets/data/interim-full-daily/?levtype=pl))
You can get them using ECMWF API. ([https://software.ecmwf.int/wiki/display/WEBAPI/Accessing+ECMWF+data+servers+in+batch](https://software.ecmwf.int/wiki/display/WEBAPI/Accessing+ECMWF+data+servers+in+batch))

**Warning:** ECMWF library is written in Python 2.7. Use a dedicated environment to use it !

An Example of Python code to get these data is :

    # Initialisation
    from api import ECMWFDataServer
    from datetime import date, timedelta as td
    import sys
    print sys.argv
    from datetime import date, timedelta as td
    
    # Inputs
    d1 = date(2016, 4, 1)
    d2 = date(2016, 5, 10)
    
    folder="E:/ECMWF/"
    
    # Loop on days
    delta = d2 - d1
    server = ECMWFDataServer()
    for i in range(delta.days + 1):
    	d=d1 + td(days=i)
    	path=folder + d.strftime("MAAC_%Y%m%d.nc" )
    	print d.strftime("----------%Y%m%d-------------" )
    	server.retrieve({
    	"class": "mc",
    	"area" : "73.5/-27/33/45",
    	"dataset": "macc_nrealtime",
    	"date": d.strftime("%Y%m%d" ),
    	"grid": "0.125/0.125",
    	"levelist": "70/100/125/150/175/200/225/250/300/350/400/450/500/550/600/650/700/750/775/800/825/850/875/900/925/950/975/1000",
    	"levtype": "pl",
    	"param": "129.128/130.128/133.128/157.128",
    	"step": "0",
    	"stream": "oper",
    	"format" : "netcdf",
    	"target": path,
    	"time": "00/06/12/18",
    	"type": "an",
    	})


If you are behind a corporate proxy, you need to specify the password in the file `api.py` (around line 325)

----------
By M.Hervo, MeteoSwiss/E-PROFILE
2015-2016