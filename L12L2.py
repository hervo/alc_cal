#!/usr/bin/python

# -*- coding: utf-8 -*-
"""
L12L2

Script to convert L1 Files in L2.
All files in folder_input will be converted in L2 and saved in folder_output.
The folder containing Lidar constants is described by the variable folder_calibration 

Data format Documentation: E-PROFILE_ALC_data_format_v06-1.docx
Raw2L1 and L12L2 current issues: Raw2L1-L12L2_Issues_and_developments_v1-1.docx

Created on Fri Oct 21 09:48:15 2016
@author: Maxime Hervo MeteoSwiss

Modifed by Myles Turp to expect the directory name as an input and additional error trapping.

"""

from __future__ import print_function, division, absolute_import
from netCDF4 import Dataset
import numpy as np
import glob
from utils import nans
import ntpath
import datetime 
import os
import sys
import string


def GenerateL2Files(directory_name):

    """
    Function to generate L2 files
   
    Keyword arguments:
       directory_name
       
    Returns:
       None
       
    Modules:
       netcdf.
       
    Comments:
       Uses input directory name 
    """
    
    # The raw and L1 files can be found in the following location
    fpath="/data/local/rsmt/e-profile/" + directory_name
    fpath="C:/DATA/Ceilometer/E-PROFILE/L1_FILES/" + directory_name
    print(fpath)
    
    # %% Inputs
    info={'folder_input': fpath + "/L1_FILES/", 
       'folder_output': fpath + "/L2_FILES/",
       'folder_calibration': fpath + "/CALIB_FILES/",
       'resol_time':5,# Time resoltion In Minutes
       'resol_altitude':30, # Vertical resolution in meters
       'L12L2_version': '0.1.3' ,
       }
 
    
    print( info)
    
    # info={'folder_input':"/data/local/rsmt/e-profile/201611080000-201611090000/L1_FILES/", 
    #    'folder_output':"/data/local/rsmt/e-profile/201611080000-201611090000/L2_FILES/",
    #    'folder_calibration':"/data/local/rsmt/e-profile/201611080000-201611090000/CALIB_FILES/",
    #    'resol_time':5,# Time resoltion In Minutes
    #    'resol_altitude':30, # Vertical resolution in meters
    #    'plot':0,
    #    'L12L2_version': '0.1.2' ,
    #    }   
    
    # %% Check If directory exist    
    if not os.path.exists( info['folder_output']   ):
        os.makedirs( info['folder_output'])

    # %% Loop on files
    nc_file =  info["folder_input"] +   "L1_*.nc" 
    file_list =glob.glob(nc_file)
    for f in range(0, len(file_list)):
       print(file_list[f])
       
       # get current date timestamp. MT
       
       now = datetime.datetime.now()
       
       date_now = now.strftime("%d/%m/%y %H:%M:%S")
    
       # %% Read DAta
       data = Dataset(file_list[f])
    
       try:
          # Read attributes
          instrument_id = data.instrument_id
          instrument_firmware_version = data.instrument_firmware_version
          instrument_type = data.instrument_type

          station_latitude = data.variables['station_latitude'][:]  
          station_longitude = data.variables['station_longitude'][:]  
          station_altitude = data.variables['station_altitude'][:] 
       
       
	      
          # Read time 
          time = data.variables['time'][:]

          # Read range and altitude
          range_alc = data.variables['range'][:]
          z = range_alc + station_altitude


          # Read Range corrected signal
          rcs = data.variables['rcs_0'][:]

          # Read Cloud base height
          cloud_base_height = data.variables['cloud_base_height'][:]

          cloud_base_height = cloud_base_height.astype(float)
          if not(type(cloud_base_height)==np.ndarray):
              # Convert masked array to Classic arrays
              cloud_base_height = cloud_base_height.filled(np.nan)
          cloud_base_height[cloud_base_height<0] = np.nan;
    
          # Read Cloud cover
          cloud_amount = data.variables['cloud_amount'][:]   

          cloud_amount = cloud_amount.astype(float)
          if not(type(cloud_amount) == np.ndarray):
              # Convert masked array to Classic arrays
              cloud_amount = cloud_amount.filled(0)
        
 
          tilt_angle = data.variables['tilt_angle'][:]  
          wavelength = data.variables['l0_wavelength'][:] 
       
       except:
           print(date_now + " LIDARNET ALL ALL HERMES_E-PROFILE DATA_ERROR 120 HARMLESS 1 FALSE UNABLE TO READ DATA ATTRIBUTES FROM L1 File " + file_list[f])
	        
       # Close netcdf file
       data.close
    
    
       # %% Average Data
       resol_time=info["resol_time"]/24/60; # Convert minutes in Days
       time_mean = np.arange(min(time), max(time), resol_time)
       range_alc_mean = np.arange(min(range_alc), max(range_alc),info['resol_altitude'])    

       rcs_mean_tmp = nans((len(time_mean), len(range_alc)))
       rcs_mean  =nans((len(time_mean), len(range_alc_mean)))

       cbh_mean = nans((len(time_mean), 3))
       cloud_amount_mean = nans((len(time_mean), 1))
       
   
       for t in range(0,len(time_mean)):
           index = np.logical_and(time>=time_mean[t], time <time_mean[t]+resol_time)
           rcs_mean_tmp[t,:] = np.mean(rcs[index,:],axis=0)
   
        # add try except block as code can fail here
        
       try:
           cbh_mean[t,:]  = np.nanmin(cloud_base_height[index,0:3],0)
            # Consider that the cloud base height is the lowest altitude of each cloud layer
       except:
            print(date_now + " LIDARNET ALL ALL HERMES_E-PROFILE DATA_ERROR 121 HARMLESS 1 FALSE  UNABLE TO CALCULATE CLOUD BASE HEIGHT MEAN " + file_list[f])
            #print("10/11/16 00:04:19 LIDARNET ALL ALL HERMES_E-PROFILE DATA_ERROR 113 HARMLESS 1 FALSE ERROR UNABLE TO CALCULATE CLOUD BASE HEIGHT MEAN")

   
       cloud_amount_mean[t] = np.round(np.mean(cloud_amount[index]))
	   
   
       for i in range(0,len(range_alc_mean)):     
          index = np.logical_and(range_alc>=range_alc_mean[i], range_alc<range_alc_mean[i]+info['resol_altitude'])
          rcs_mean[:,i] = np.mean(rcs_mean_tmp[:,index],  axis=1)
       

       # %% Read and Apply Calibration
       basename = ntpath.basename(file_list[f])
       WMO_str = basename[3:8]
       identifier = basename[9]
       date_str = basename[10:22]
       file_calibration_tmp = info['folder_calibration'] +   'ALC_calibration_' + WMO_str + '_' + identifier + '_*.nc'
       file_calibration =glob.glob(file_calibration_tmp)
    
       if len(file_calibration)>0:
          data_cal = Dataset(file_calibration[0])
          CL_all = data_cal['lidar_constant']
          CL = np.median (CL_all)
          data_cal.close
        
       else:
          if instrument_type == 'CL31' :     
            CL=1e8
          elif instrument_type == 'CL51' :     
            CL=1e8
          elif instrument_type == 'CHM15k': 
            CL=3e11
    
       # Aplly Calibatrion and Convert in Mm-1
       attenuated_backscatter = rcs_mean / CL * 1e6
    

    
       # %% Calculate flags and uncertainties
       attenuated_backscatter_uncertainties=abs(attenuated_backscatter*0.25);
       cbh_uncertainties = np.empty((len(time_mean),3),  dtype=float)
       cbh_uncertainties.fill(50)
       flag=np.zeros(np.shape(attenuated_backscatter)); 
       is_cloud = ~np.isnan(cbh_mean)
       if  is_cloud.any():        
           for i in range(0,len(time_mean)):
               flag[i,range_alc_mean>max(cbh_mean[i]+1000)]=1;
                
       # %% Write L2 File
    
       output_file = info['folder_output'] + 'L2_' + WMO_str + "_" + identifier + date_str + '.nc'
    
       print("Creating NetCDF file: " + output_file)

       # create a file (Dataset object, also the root group).
       ncID = Dataset(output_file, 'w', format='NETCDF4')
    
       # dimensions.
       nc_time = ncID.createDimension('time', None)
       nc_altitude = ncID.createDimension('altitude', len(range_alc_mean))
       nc_layer = ncID.createDimension('layer', 3)

    
       # Copy Global attibute
       attribute_list = data.ncattrs()
       for nc_attr in attribute_list:
           ncID.setncattr(nc_attr,data.getncattr(nc_attr))
       history = data.history + " / " + datetime.datetime.now().strftime("%Y%m%d") + ' L12L2 ' + info['L12L2_version'] 
       ncID.setncattr ('history',history) 

       # Create variables.
       # You must use zlib=true to preserve an siiiiignificant amount of disk space
       nc_time = ncID.createVariable('time', 'f8', ('time',), zlib=True)
       nc_time.long_name = "End time (UTC) of the measurements"
       nc_time.units = "days since 1970-01-01 00:00:00.000"
       nc_time.standard_name = "time"
       nc_time.calendar = "gregorian"
       nc_time[:] = time_mean

       nc_start_time = ncID.createVariable('start_time', 'f8', ('time',), zlib=True)
       nc_start_time.long_name = "Start time (UTC) of the measurements"
       nc_start_time.units = "days since 1970-01-01 00:00:00.000"
       nc_start_time[:] = time_mean-info['resol_time']
    
       nc_altitude = ncID.createVariable('altitude', 'f8', ('altitude',), zlib=True)
       nc_altitude.long_name = "Altitude above sea level"
       nc_altitude.units = "m"
       nc_altitude.standard_name = "altitude"
       altitude =  np.mean(np.cos(np.deg2rad(tilt_angle)))*range_alc_mean +station_altitude
       nc_altitude[:] = altitude
    
       nc_latitude = ncID.createVariable('latitude', 'f8', ('altitude','time'), zlib=True)
       nc_latitude.long_name = "Latitude for each measurement"
       nc_latitude.units = "degrees north"
       fake_lat = np.empty((len(range_alc_mean),len(time_mean)),  dtype=float)
       fake_lat.fill(np.nan)
       nc_latitude[:,:] = fake_lat
    
       nc_longitude = ncID.createVariable('longitude', 'f8', ('altitude','time'), zlib=True)
       nc_longitude.long_name = "Longitude for each measurement"
       nc_longitude.units = "degrees east"
       nc_longitude[:,:] = fake_lat

    
       nc_beta_att = ncID.createVariable('attenuated_backscatter_0', 'f8', ('altitude','time'), zlib=True)
       nc_beta_att.long_name = "Attenuated Backscatter at wavelength 0"
       nc_beta_att.units = "m^-1.sr^-1"
       nc_beta_att[:,:] = np.transpose(attenuated_backscatter)

    
       nc_un_beta_att = ncID.createVariable('uncertainties_att_backscatter_0', 'f8', ('altitude','time'), zlib=True)
       nc_un_beta_att.long_name = "Uncertainties for Attenuated Backscatter at wavelength 0"
       nc_un_beta_att.units = "m^-1.sr^-1"
       nc_un_beta_att[:,:] = np.transpose(attenuated_backscatter_uncertainties)

    
       nc_wavelength = ncID.createVariable('l0_wavelength', 'f8', zlib=True)
       nc_wavelength.long_name = "Wavelength of Laser for channel 0"
       nc_wavelength.units = "nm"
       nc_wavelength[:] = wavelength

    
       nc_station_longitude = ncID.createVariable('station_longitude', 'f8', zlib=True)
       nc_station_longitude.long_name = "Longitude of measurement station"
       nc_station_longitude.units = "degrees_east"
       nc_station_longitude.standard_name = "longitude"
       nc_station_longitude[:] = station_longitude

    
       nc_station_latitude = ncID.createVariable('station_latitude', 'f8', zlib=True)
       nc_station_latitude.long_name = "Latitude of measurement station"
       nc_station_latitude.standard_name = "latitude"
       nc_station_latitude.units = "degrees_north"
       nc_station_latitude[:] = station_latitude

    
       nc_station_altitude = ncID.createVariable('station_altitude', 'f8', zlib=True)
       nc_station_altitude.long_name = "Altitude of measurement station"
       nc_station_altitude.units = "m"
       nc_station_altitude[:] = station_altitude


       nc_flag = ncID.createVariable('quality_flag', 'i', ('altitude','time'), zlib=True)
       nc_flag.long_name = "Quality flag: To be defined"
       nc_flag.flag_values = "0,1,2"
       nc_flag.flag_meanings = "OK,Do no use,To be defined"
       nc_flag[:] = np.transpose(flag)


       nc_cloud_base_height = ncID.createVariable('cloud_base_height', 'f8', ('time','layer'), zlib=True)
       nc_cloud_base_height.long_name = "Cloud Base Height above ground level"
       nc_cloud_base_height.units = "m"
       nc_cloud_base_height[:] = cbh_mean

    
       nc_cbh_uncertainties = ncID.createVariable('cbh_uncertainties', 'f8', ('time','layer'), zlib=True)
       nc_cbh_uncertainties.long_name = "Uncertainties for Cloud Base Height"
       nc_cbh_uncertainties.units = "m"
       nc_cbh_uncertainties[:] = cbh_uncertainties

    
       nc_cloud_amount = ncID.createVariable('cloud_amount', 'f8', ('time',), zlib=True)
       nc_cloud_amount.long_name = "Cloud amount"
       nc_cloud_amount.units = "Octa"
       nc_cloud_amount[:] = cloud_amount_mean

    
       nc_calibration_constant_0 = ncID.createVariable('calibration_constant_0', 'f8', ('time',), zlib=True)
       nc_calibration_constant_0.long_name = "Value of the calibration constant used to calculate the attenuated back scatter at wavelength 0"  
       if instrument_type == 'CL51':
           nc_calibration_constant_0.units = "V* m^3 / sr"
       elif instrument_type== 'CL31':
           nc_calibration_constant_0.units = "V* m^3 / sr"
       elif instrument_type == 'CHM15k':
           nc_calibration_constant_0.units = "counts/s * m^3 / sr"
       else:
           print(date_now + " LIDARNET ALL ALL HERMES_E-PROFILE DATA_ERROR 122 HARMLESS 1 FALSE INSTRUMENT TYPE UNKNOWN in L1 file " + file_list[f])
           raise NotImplementedError(" Implement Units for this instrument")
       nc_calibration_constant_0[:] = CL

    
       ncID.close()

def raw2l1_wrapper(argv):

   """ Description of the Main program loop
    
    @Version: 1.0
    @Author: Maxime Hervo - modified by Myles Turp to work off input files
    
    Example usage:
    python2.7 L12L2_MT.py 201611080000-201611090000
                             
    
   """
   if len(sys.argv) != 2:  # the program name and the two arguments
   # stop the program and print an error message
       sys.exit("Must provide the sub-directory name as an input e.g. 201604110000-201604120000")

    # Look for files containing site information
    
   dname = sys.argv[1]
   
   print (dname)
 
   # Use the dates as an input for the directory names
   GenerateL2Files(dname)
   
   
if __name__ == "__main__":
    
    ini_filename = raw2l1_wrapper(sys.argv)
    #print ini_filename  
        
else:
    print("main is being imported into another module")  
