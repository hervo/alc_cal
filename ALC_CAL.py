# -*- coding: utf-8 -*-
#
# Warning It does not work with L1 files from UK met office
# UK Met provides NOT-Range-Corrected-Signal instead of RCS



from __future__ import print_function, division, absolute_import

from ALC_cal_rayleigh_tools_v3 import alc_cal_rayleigh
from datetime import timedelta, date
from utils import nans
from matplotlib.pyplot import *
from matplotlib import cm
import numpy as np
import json

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

close('all')

#%%  Inputs """
start_date = date(2017,1,15)
end_date = date(2017,2,6 )

# Define instrument information
with open('E-PROFILE_instruments_L2.json') as data_file:    
    info = json.load(data_file)


# Define Rayleigh calibration options
with open('E-PROFILE_options_calibration.json') as data_file_options:    
    options = json.load(data_file_options)
	


# Flag meanings    
flag_list={ '1':'Successfull',
    '0.5':'is_partially_clear_night',
    '-1':'Not a clear night',
    '0':'No data',
    '-2':'Signal is not proportional to Molecular scattering (Rayleigh fit give different results compared to Median Sl/Pmol)',
    '-3':'Other quality ratio',
    '-4':'Missing Model data',
    '-5':'RCS contains only NaN',
    '-6':'Uncertainties higher than CL Values',
    '-7': 'Negative Rayleigh fit',
    '-8': 'Rayleigh fit issue: a is Larger than b'}    


#%% Loop on station
#calc length of date range for preallocation 
N=1
days=[]
for single_date in daterange(start_date, end_date):
    N=N+1
days=[x for x in range(0,N)]
time=[x for x in daterange(start_date, end_date)]
CL=nans((len(info),N))
flag=nans((len(info),N))
Error_CL = nans((len(info),N))

for s in range(0,len(info)):
    print('------------------------------   ' + info[s]['SiteName'] + '  ---------------------------------' )
    i=0
    for single_date in daterange(start_date, end_date):
        date_str= single_date.strftime("%Y%m%d")
        print('-------------------------   ' +date_str+ '  -----------------------------' )
        if info[s]["Type"] == 'CL51':
            print('No calibration')
        elif info[s]["Type"] == 'CL31':
            print('No calibration')
        elif info[s]["Type"] == 'CHM15k':
            CL[s,i], flag[s,i],Error_CL[s,i]=alc_cal_rayleigh(date_str,info[s],options)  
        else:
            raise NotImplementedError(" Implement Calibration for this instrument")        
        i=i+1
CL[CL==-1]=np.nan
Error_CL[Error_CL==-1]=np.nan
#%% Plot outputs 


figure(figsize=(15,15))

ax1=subplot(2,1,1)
xlim(0,N+10) 
grid('on')

ax2=subplot(2,1,2)
xlim(0,N+10) 
grid('on')
xlabel('Days since ' + start_date.strftime("%d-%m-%Y"))

idx = np.linspace(0, 1, len(info))
colorvec=cm.jet(idx)
width=0.1;

for s in range(0,len(info)):
    days2=[x+width*s/2 for x in range(0,N)]
    if not(np.all(np.isnan(CL))):
        ax1.errorbar(days,CL[s,:],yerr=Error_CL[s,:]*CL[s,:]/100 , label=info[s]['SiteName'],color=colorvec[s,:], fmt='-s')
    
    ax2.bar(days2,flag[s,:],width=width, label=info[s]['SiteName'],color=colorvec[s,:], edgecolor = "none")
    

ax1.legend(loc = 'center right')   
ax2.legend(loc = 'center right') 
if not(np.all(np.isnan(CL))):
    ax1.set_yscale('log')
# set flag labels
flag_number=(-8,-7,-6,-5,-4,-3,-2,-1,0,0.5,1)   
flag_meaning=('Rayleigh fit issue: a is Larger than b','Negative Rayleigh fit','Uncertainties higher than CL Values','RCS contains only NaN','Missing Model data','Other quality ratio','Bad Quality ratio','Not a clear night','No data','is_partially_clear_night','Successfull')
yticks(flag_number,flag_meaning)


if np.all(np.isnan(CL)):
    raise NotImplementedError("No CL determined")

#%% Create interactive plot
import bokeh.plotting as bk
def rgb_to_hex(rgb):
    return '#%02x%02x%02x' % rgb
    
f=bk.figure( tools="pan,box_zoom,reset,save,hover")
bk.output_file("calibration_output.html", title="calibration output")    

for s in range(0,len(info)):
    color_hex=rgb_to_hex((int(np.round(colorvec[s,0]*255)),int(np.round(colorvec[s,1]*255)),int(np.round(colorvec[s,2]*255))))
    f.circle(days,CL[s,:],color=color_hex,legend=info[s]['SiteName'])
bk.show(f)


