# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 11:01:49 2015

@author: hem
"""
import numpy 

def nans(shape, dtype=float):
    a = numpy.empty(shape, dtype)
    a.fill(numpy.nan)
    return a